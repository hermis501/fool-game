<?php

/** 
 * This file is a Bootstrap that includes and holds all necessary components
 * @author Fool Online team
 *@package Fool Online
 */

ini_set('display_startup_errors', true);
ini_set('display_errors', true);
error_reporting(E_ALL);
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
setlocale(LC_ALL, 'lt_LT');

// Important constants
define('ROOT_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR); // Default root directory
define('CORE_DIR', ROOT_DIR . 'core' . DIRECTORY_SEPARATOR); // All Core includes
define('APP_DIR', ROOT_DIR . 'App' . DIRECTORY_SEPARATOR); // Application
define('LIB_DIR', ROOT_DIR . 'lib' . DIRECTORY_SEPARATOR); // All integrated libraries
define('WWW_DIR', dirname(ROOT_DIR) . DIRECTORY_SEPARATOR . 'html' . DIRECTORY_SEPARATOR); // Public
define('MODELS_DIR', APP_DIR . 'models' . DIRECTORY_SEPARATOR); //  Models directory
define('CONTROLLERS_DIR', APP_DIR . 'controllers' . DIRECTORY_SEPARATOR); // All controllers
define('LOGS_DIR', ROOT_DIR . 'logs' . DIRECTORY_SEPARATOR); // All errors that been logged at run-time
define('SMARTY_DIR', LIB_DIR . 'smarty' . DIRECTORY_SEPARATOR . 'smarty_lib' . DIRECTORY_SEPARATOR); // We use Smarty for templateing
define('PHP_MAILER_DIR', LIB_DIR . 'PHPMailer' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR);

// Includes
require_once APP_DIR . 'config.php';
require_once CORE_DIR . 'pdo.php';
require_once CORE_DIR . 'router.php';
require_once CORE_DIR . 'loader.php';
require_once CORE_DIR . 'Controller.php';
require_once SMARTY_DIR . 'Smarty.class.php';
require_once CORE_DIR . 'json_parser.php';
require_once CORE_DIR . 'error.php';
require_once CORE_DIR . 'session.php';
require_once CORE_DIR . 'validation.php';
require_once CORE_DIR . 'form_error_collector.php';
require_once CORE_DIR . 'role.php';

$smarty = Core\Controller::loadModel('\Smarty', [], true); // Global views object
$smarty->setTemplateDir(APP_DIR . 'views');
$smarty->setCompileDir(LIB_DIR . 'smarty' . DIRECTORY_SEPARATOR . 'templates_c');
$smarty->setConfigDir(LIB_DIR . 'smarty' . DIRECTORY_SEPARATOR . 'configs');
$smarty->setCacheDir(LIB_DIR . 'smarty' . DIRECTORY_SEPARATOR . 'cache');
$smarty->addPluginsDir(LIB_DIR . 'smarty' . DIRECTORY_SEPARATOR . 'custom_pluggins');
//$smarty->debugging = true; // Comment on production
$smarty->compile_check = true;
//$smarty->force_compile = true; // Remove at production

$errorController = new Core\Error(LOGS_DIR, $smarty); // Set log directory to be used for uncaught exceptions and pass Smarty object to be used as a view Controller
set_exception_handler([$errorController, 'handleExceptions']);

$sessionHandler = new Core\Session();
session_set_save_handler($sessionHandler, true); // Take the control of the sessions, save them into database instead of filesystem and register session_shutdown to invoke session_write_close()
session_start();

spl_autoload_register('Core\loader::autoLoadClass'); // Auto loading class mechanizm, it supports namespaces by default

if (!defined('SERVER_ERROR')) {
    $user = Core\Controller::loadModel('\App\Models\User', [\App\Models\User::ACTION_AUTHENTICATE], true);
    $role = Core\Controller::loadModel('Core\Role', [Core\Role::USER], true); // Has no particular roles
    if ($user->isAuthorized()) { // Has access to the logged-in user areas
        $role->setRole($user->getRole());
        $smarty->assign('user_authorized', true);
    } else {
        $smarty->assign('user_authorized', false);
    }
}
