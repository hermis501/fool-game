<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.display_current_year.php
 * Type:     function
 * Name:     display_current_year
 * Purpose:     Outputs a current year
 * -------------------------------------------------------------
 */
	
function smarty_function_display_current_year($params, Smarty_Internal_Template $template) {
	return date('Y');
}