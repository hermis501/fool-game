<?php

/** 
 * Class rewrites default PHP behaviour to keep Sessions in filesystem
 * Instead of keeping Session in FS, class stores Sessions in database
 */

namespace Core;

class Session implements \SessionHandlerInterface
{
    const SESSION_NAME = 'User-auth-identification-ACC'; // change a session name for security reasons
    const AUTHORIZATION_COOKIE_NAME = 'Auth-usr-token'; // User Login Token Cookie Name
    const REMEMBER_ME_COOKIE_NAME = 'remember-me'; // Cookie name for remember me functionality
    const USER_SESSION_NAME = 'Auth-User-ID'; // Session name that refers to User's ID
    const SECURE = false; // secure mode  can be used with https
    const HTTP_ONLY = true; // if this flag is true, cookies goes only via http, although not all browsers supports this mode, it protects from the Javascript XSS atacks
    const COOKIE_LIFETIME = 3600; // Cookies and Session by default expires in 1 hour
    const REMEMBER_ME_COOKIE_LIFETIME = 2592000; // If remember me is set (time to apply = 30 days)

    /**
     * 
     * @var MyPDO
     */
    protected $db;

    /**
     * Session data
     *
     * @var string[]
     */
    protected $data;

    /**
     * Sets session cookie params
     * 
     * @throws Exception
     */
    public function __construct()
    {
        if (!ini_set('session.use_only_cookies', true)) { // if there is impossible to set that session_id would be passed only by cookies, not via URL
            throw new \Exception('Session is not set via Cookie and this is not a safe method.');
        }

        if (!ini_set('session.gc_maxlifetime', self::getSessionCookieLifetime())) { // set  time for the session garbage collector on the server
            throw new \Exception('Unable to set Garbage Collector Session max life time.');
        }

        $cookieParams = session_get_cookie_params(); // get cookie parameters
        session_set_cookie_params(self::getSessionCookieLifetime(), $cookieParams['path'], $cookieParams['domain'], self::SECURE, self::HTTP_ONLY); // set cookie parameters that will be used with sessions
        session_name(self::SESSION_NAME); // set previously given session_name
    }

    /**
     * Undocumented function
     *
     * @param string $savePath
     * @param string $sessionName
     * @return boolean
     */
    public function open($savePath, $sessionName)
    {
        $this->db = MyPDO::instance();
        $cookieParams = session_get_cookie_params();
        setcookie(session_name(), session_id(), time() + self::getSessionCookieLifetime(), $cookieParams['path'], $cookieParams['domain'], self::SECURE, self::HTTP_ONLY); // Roughly update PHPSESSID cookie to endure during all client requesting process
        if (!$this->gc()) { // Force garbage collector to automatically check for old sessions and if there are some, remove
            return false; // If old sessions weren't deleted, terminate the process of initiating Sessions
        }

        return true;
    }

    /**
     * Closes Session
     *
     * @return void
     */
    public function close()
    {
        $this->db = NULL;
        return true;
    }

    /**
     * Reads session content from DB
     * Holds all content in class property
     *
     * @param string $sessionId
     * @return string
     */
    public function read($sessionId): string
    {
        $query = "SELECT session_data FROM session WHERE id = :session_id LIMIT 1";
        $stmt = $this->db->query($query, [':session_id' => $sessionId]);
        $this->data = $stmt->fetch();
        if (!$this->data) {
            return ''; // Return empty string if there is nothing to return
        }

        return $this->data['session_data']; // Populate $_SESSION array
    }

    /**
     * PHP calls this method when tries to save $_SESSION array
     *
     * @param string $sessionId
     * @param string $sessionData
     * @return boolean
     */
    public function write($sessionId, $sessionData)
    {
        $query = "REPLACE INTO session (id, session_expiration_datetime, session_data)";
        $query .= " VALUES(:session_id, DATE_ADD(NOW(), INTERVAL :session_expiration_datetime SECOND), :session_data)";
        $stmt = $this->db->query($query, [':session_id' => $sessionId, ':session_expiration_datetime' => self::getSessionCookieLifetime(), ':session_data' => $sessionData]);
        if (!$stmt)
            return false;

        return true;
    }

    /**
     * Destroys existing session given by it's ID
     *
     * @param string $sessionId
     * @return boolean
     * Always return true to insure that PHP handles Sessions properly
     */
    public function destroy($sessionId)
    {
        $query = "DELETE FROM session WHERE id = :session_id LIMIT 1";
        $this->db->query($query, [':session_id' => $sessionId]);
        return true;
    }

    /**
     * Collects garbage Session data
     *
     * @param int $sessionLifetime
     * @return boolean
     */
    public function gc($sessionLifetime = null)
    {
        $query = "DELETE FROM session WHERE NOW() > session_expiration_datetime"; // Remove expired sessions
        $this->db->query($query);
        return true;
    }

    /**
     * Gets Session Cookie lifetime
     *
     * @return integer
     */
    public static function getSessionCookieLifetime(): int
    {
        if (isset($_COOKIE[\Core\Session::REMEMBER_ME_COOKIE_NAME])) {
            return self::REMEMBER_ME_COOKIE_LIFETIME;
        }
        return self::COOKIE_LIFETIME;
    }
}
