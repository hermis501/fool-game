<?php

/** 
 * Basic input validation
 */

namespace Core;

class Validation
{
    /**
     * All fields that coming from $_POST or $_GET
     * @var array
     */
    private $fields;

    /**
     *
     * @var FormErrorCollector
     */
    private $errors;

    /**
     *
     * @param array $params
     * @param string $requestMethod
     * @throws Exception
     */
    public function __construct(array $params, string $requestMethod = 'POST')
    {
        $this->errors = FormErrorCollector::instance();
        $this->fields = $params;
        $possibleTypes = ['str', 'int', 'bool', 'multiple'];
        $possibleMethods = ['GET', 'POST'];
        $requiredProperties = [
            'name' => 'string',
            'type' => $possibleTypes,
            'min' => 'int',
            'max' => 'int',
            'pattern' => '', // check happens in auto validation
            'identical_with' => '', // If two or more fields should be tested as identical - ===
            'not_identical_with' => '', // If two or more fields should be tested as not identical - !==
            'value' => ''
        ];
        if (!in_array($requestMethod, $possibleMethods)) {
            throw new \Exception('Validation class Constructor got unexpected method parameter.');
        }

        foreach ($this->fields as $postName => &$field) {
            $field['identical_with'] = isset($field['identical_with']) ? $field['identical_with'] : '';
            $field['not_identical_with'] = isset($field['not_identical_with']) ? $field['not_identical_with'] : '';
            if (empty($postName)) {
                throw new \Exception('Validation class Constructor detected unset parameter "post name".');
            }

            if (empty($field['name'])) {
                throw new \Exception('Validation class Constructor detected unset parameter "name".');
            }

            if (count(array_intersect_key($requiredProperties, $field)) !== count($requiredProperties)) { // checks if there are same pares of keys within required and passed array
                throw new \Exception('Validation Constructor got incorrect parameters about the fields needed to be valid.');
            }

            if (!in_array($field['type'], $requiredProperties['type'])) {
                throw new \Exception('Validation class has no given type of field ' . $field['type'] . '.');
            }

            $field['min'] = is_int($field['min']) ? abs($field['min']) : 0; // If not given, it will be treated like false / no-required 
            $field['max'] = is_int($field['max']) ? abs($field['max']) : 0; // If not given, it will be treated like value length is unlimited

            if ($requestMethod === 'POST') {
                $field['value'] = isset($_POST[$postName]) && is_string($_POST[$postName]) ? $_POST[$postName] : $params[$postName]['value'];
            } else { // GET
                $field['value'] = isset($_GET[$postName]) && is_string($_GET[$postName]) ? $_GET[$postName] : $params[$postName]['value'];
            }
        }

        unset($field);
        $this->checkFields();
    }

    private function checkFields()
    {
        foreach ($this->fields as $postName => $field) {
            // Check emptyness
            if ($field['min'] !== 0 && self::isEmpty($field['value'])) { // min !== 0 - field is required
                $this->errors->add('Field &quot;' . $field['name'] . '&quot; cannot be empty.');
            }
            // Check length
            elseif ($field['min'] > 0 && self::getLength($field['value']) < $field['min']) {
                $this->errors->add('Field &quot;' . $field['name'] . '&quot; is too short, at least ' . $field['min'] . ' characters.');
            } elseif ($field['max'] > 0 && self::getLength($field['value']) > $field['max']) {
                $this->errors->add('Field &quot;' . $field['name'] . '&quot; is too long, not more than ' . $field['max'] . ' characters are allowed.');
            } else {
                // Regex
                switch ($field['pattern']) {
                    case 'alpha':
                        if (!self::isAlpha($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can only consist of latin letters.');
                        }
                        break;

                    case 'alphanumeric':
                        if (!self::isAlphanumeric($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can consist of latin letters and arabic digits.');
                        }
                        break;

                    case 'alphaSpace':
                        if (!self::isAlphaSpace($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can only consist of Latin letters and spaces.');
                        }
                        break;

                    case 'alphaDash':
                        if (!self::isAlphaDash($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can consist  of latin letters and dash (-) character.');
                        }
                        break;

                    case 'alphanumericDash':
                        if (!self::isAlphanumericDash($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can consist of latin letters, arabic digits and dash (-) character.');
                        }
                        break;

                    case 'numeric':
                        if (!self::isNumeric($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can only consist of arabic digits.');
                        }
                        break;

                    case 'email':
                        if (!self::isEmail($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; is invalid.');
                        }
                        break;

                    case 'password':
                        if (!self::isPassword($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; should consist of at least one arabic digit, at least one capital and at least one lowercase latin letter and should be at least ' . $field['min'] . ' characters of length.');
                        }
                        break;

                    case 'ltName': // Names with Lithuanian letters
                        if (!self::isLithuanianName($field['value'])) {
                            $this->errors->add('Field &quot;' . $field['name'] . '&quot; can only consist of Latin letters, Lithuanian letters and space character.');
                        }
                        break;

                    case 'post': // For later if Validation will be required
                        break;

                    case 'none':
                        break;

                    default:
                        throw new \Exception('Invalid Regular Expression parameter.');
                }

                if ($field['identical_with'] !== '' && $field['value'] !== $this->fields[$field['identical_with']]['value']) { // Field must be identical with some other field
                    $this->errors->add('Fealds &quot;' . $field['name'] . '&quot; and &quot;' . $this->fields[$field['identical_with']]['name'] . '&quot; values are not the same.');
                } elseif ($field['not_identical_with'] !== '' && $field['value'] === $this->fields[$field['not_identical_with']]['value']) { // Field must not be identical with some other field
                    $this->errors->add('Fields &quot;' . $field['name'] . '&quot; and &quot;' . $this->fields[$field['not_identical_with']]['name'] . '&quot; cannot have the same value.');
                }
            }
        }
        unset($field);
    }

    /**
     * Gets value of the given field (by it's name)
     *
     * @param string $fieldName
     * @return string
     */
    public function getFieldValue(string $fieldName): string
    {
        if (!array_key_exists($fieldName, $this->fields)) {
            return false; // Actually there is no such field
        }

        return $this->fields[$fieldName]['value'];
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Gets all the values of the given fields
     *
     * @param array $fields
     * Must be 2-dimentional
     * @return array
     * @throws Exception
     */
    public static function getFieldsValues(array $fields): array
    {
        $values = [];
        foreach ($fields as $postName => $field) {
            if (!is_array($field)) {
                throw new \Exception('getFieldsValues() expects parameter to be 2-dimentional associative array.');
            }
            if (!isset($field['value'])) {
                throw new \Exception('getFieldsValues() expects parameter to have member "Value".');
            }
            $values[$postName] = $field['value'];
        }

        return $values;
    }

    public static function isAlpha(string $var): bool
    {
        return preg_match('/^[A-Za-z]*$/', $var);
    }

    public static function isAlphaSpace(string $var): bool
    {
        return preg_match('/^[A-Za-z\40]*$/', $var);
    }

    public static function isAlphaDash(string $var): bool
    {
        return preg_match('/^[A-Za-z-]*$/', $var);
    }

    public static function isAlphanumericDash(string $var): bool
    {
        return preg_match('/^[A-Za-z0-9-]*$/', $var);
    }

    public static function isAlphanumeric(string $var): bool
    {
        return preg_match('/^[A-Za-z0-9]*$/', $var);
    }

    public static function isNumeric(string $var): bool
    {
        return preg_match('/^[0-9]*$/', $var);
    }

    public static function isEmail(string $var): bool
    {
        return filter_var($var, FILTER_VALIDATE_EMAIL);
    }

    public static function isPassword(string $var): bool
    {
        return preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{6,}$/', $var); // At least 1 Cap, 1 low Latin letter, 1 Arabic digit
    }

    public static function isLithuanianName(string $var): bool
    {
        return preg_match('/^[A-Za-z\40ĄČĘĖĮŠŲŪŽąčęėįšųūž]*$/', $var);
    }

    public static function isEmpty(string $var)
    {
        if ($var === '') {
            return true;
        }

        return false;
    }

    public static function getLength(string $var): int
    {
        return mb_strlen($var);
    }
}
