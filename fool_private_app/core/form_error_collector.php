<?php

namespace Core;

final class FormErrorCollector
{
    /**
     * Holds all the error messages that will be printed to the user
     *
     * @var string[]
     */
    private $errors = [];

    /**
     * Errors instance
     *
     * @var FormErrorCollector
     */
    private static $hInstance = null;

    /**
     * Prevent invoking class from outside
     */
    private function __construct()
    {
    }

    /**
     * Prevent cloning
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Global errors object instance
     *
     * @return FormErrorCollector
     */
    public static function instance(): FormErrorCollector
    {
        if (!self::$hInstance) {
            self::$hInstance = new FormErrorCollector;
        }

        return self::$hInstance;
    }

    /**
     * Adds user-error message to the errors buffer
     *
     * @param string $errorMsg
     * @return void
     */
    public function add(string $errorMsg)
    {
        $this->errors[] = $errorMsg;
    }

    /**
     * Checks if errors buffer has errors that should be shown to the user
     *
     * @return boolean
     */
    public function hasErrors(): bool
    {
        if (empty($this->errors)) {
            return false; // Has no errors
        }

        return true;
    }

    /**
     * Returns all the errors in the buffer
     *
     * @return string[]
     */
    public function getErrors(): array
    {
        if ($this->hasErrors()) {
            return $this->errors;
        }

        return [];
    }
}
