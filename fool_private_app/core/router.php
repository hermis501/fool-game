<?php

/**
 * Class is responsible to load required controllers (determined by URL params) and load required view that is returned by controller
 */

namespace Core;

class Router
{
    /*
Usually  not change these constants below!!!
*/
    const MAX_URI_PARAMS = 64; // Max elements to split uri to
    const WORDS_SEPARATOR = '-'; // Dash or some other char will separate them
    const WORDS_DESEPARATOR = '_'; // Use to change word-named controller names to file-named controller names (just a preferable home style)
    const DEFAULT_CONTROLLER = 'home'; // Default controller
    const DEFAULT_ACTION = 'index'; // Default action
    const METHOD_PREFIX = 'Action'; // The given prefix to the calling method

    /**
     * Site base URL
     *
     * @var string
     */
    private $baseUrl;

    /**
     * URI that comes from the index page
     *
     * @var string
     */
    protected $uri;

    /**
     * Current set controller name
     *
     * @var string
     */
    private $controller;

    /**
     * User invoking Action
     *
     * @var string
     */
    private $action;

    /**
     * List of all parsed values from URL
     *
     * @var string[]
     */
    private $params;

    /**
     * Initiates router with given URL
     *
     * @param string $uri
     * URL that came from index.php (redirected through .htaccess)
     * @param string $baseUrl
     * Default URL
     */
    public function __construct(string $uri, string $baseUrl = null)
    {
        $this->controller = $this->formatName(self::DEFAULT_CONTROLLER);
        $this->baseUrl = $baseUrl;
        $this->uri = isset($uri) ? trim(str_replace($this->baseUrl, '', urldecode($uri)), '/') : '';
        $this->params = [];
    }

    /**
     * Gets params
     *
     * @return string[]
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     *
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Gets parameter by given index
     *
     * @param integer $paramIndex
     * @param integer $default
     * @return string
     */
    public function getParam(int $paramIndex, int $default = null): string
    {
        if (array_key_exists($paramIndex, $this->params))
            return $this->params[$paramIndex];

        return $default;
    }

    /**
     * Adds another parameter to the params list
     *
     * @param string $param
     * @return void
     */
    private function setParam(string $param)
    {
        array_unshift($this->params, $param);
    }

    /**
     * Splits URL into parts
     * Chooses controller and action to load
     *
     * @return void
     */
    public function createRequest()
    {
        $path = explode('?', $this->uri, 2); // Split to 2 parts if query string (with ? is appended)
        $uri_parts = explode('/', $path[0], self::MAX_URI_PARAMS); // Split URI into parts
        $this->controller = !empty($uri_parts[0]) ? self::formatName(array_shift($uri_parts)) : self::DEFAULT_CONTROLLER;
        $this->action = !empty($uri_parts[0]) ? self::formatName(array_shift($uri_parts)) : self::DEFAULT_ACTION;
        $this->params = $uri_parts;
    }

    /**
     * Loads controller that is being parsed by using URl param
     * returns whether it succeeded or not
     *
     * @return boolean
     */
    public function loadController(): bool
    {
        $this->createRequest();

        $controller = str_replace('_', '-', $this->getController());
        if (!Validation::isAlphaDash($controller)) {
            return false;
        }

        $action = str_replace('_', '-', $this->getAction()); // deconvert_
        if (!Validation::isAlphaNumericDash($action)) {
            return false;
        }

        $controllerClass = '\App\Controllers\\' . $controller;
        if (!class_exists($controllerClass)) { // There is no such controller as user requested
            return false;
        }

        $method = $this->getAction() . self::METHOD_PREFIX;
        if (defined($controllerClass . '::REROUT_REQUEST_TO_INDEX')) { // Given Controller wants to accept all the requests to it's index action method in order to have short URL's like /cat/news
            $method = self::DEFAULT_ACTION . self::METHOD_PREFIX;
            $action = $action === self::DEFAULT_ACTION ? '' : $action;
            $this->setParam($action);
        }

        if (!method_exists($controllerClass, $method) || !is_callable([$controllerClass, $method])) {
            return false;
        }

        $smarty = Controller::getView();
        $controllerObject = new $controllerClass();
        $viewPath = call_user_func_array([$controllerObject, $method], $this->getParams());
        if (!$viewPath) {
            return false; // Perhaps there is another (sub-controller) inside of the given controller, but parameter given to the false entry
        }

        if ($viewPath === true) { // Ajax
            define('IS_AJAX', true);
            return true;
        }

        /**
         * @todo If we will decide to keep this callculation of request time, we will need to rewrite that global param
         */
        global $startTime;
        $smarty->assign('time_elapsed', round((microtime(true) - $startTime) * 1000));
        $smarty->assign('memory_usage', round(memory_get_usage(true) / 1024 / 1024));
        $smarty->display($viewPath);
        return true;
    }

    /**
     * Converts words separated with separator to _ based. For instance 'Login-controller' would be converted to login_controller
     *
     * @param string $unformattedName
     * @return string
     */
    public static function formatName($unformattedName): string
    {
        $formattedName = str_replace(self::WORDS_SEPARATOR, self::WORDS_DESEPARATOR, $unformattedName);
        return strtolower(ltrim($formattedName));
    }
}
