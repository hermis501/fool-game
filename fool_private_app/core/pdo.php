<?php

/**
 * PDO abstraction layer for MySQL database
 */

namespace Core;

use \App\Config;
use PDO;

final class MyPDO
{
    /**
     * DB connection
     *
     * @var PDO
     */
    private $dbCon;

    /**
     * MyPDO instance for DB connection
     *
     * @var MyPDO
     */
    private static $dbInstance = NULL;

    /**
     * Connects to MySQL database
     * Private protects from invoking class
     * 
     * @throws PDOException
     */
    private function __construct()
    {
        $dsn = 'mysql:host=' . Config::DB_SERVER . ';dbname=' . Config::DB_NAME . ';charset=utf8mb4';

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_PERSISTENT => false
                    ];

        try {
            $this->dbCon = new PDO($dsn, Config::DB_USERNAME, Config::DB_PASSWORD, $options);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(),  int($e->getCode()));
        }
    }

    /**
     * Prevent cloning
     *
     * @return void
     */
private function __clone()
{
}

    /**
     * Gets MyPDO instance
     *
     * @return MyPDO
     * @throws PDOException
     */
    public static function instance(): MyPDO
    {
        if (!self::$dbInstance) {
            self::$dbInstance = new MyPDO;
        }

        return self::$dbInstance;
    }

    public function __destruct()
    {
        $this->dbCon = NULL;
        self::$dbInstance = NULL;
    }

    /**
     * Magical __call to the given method with it's parameters
     *
     * @param string $method
     * @param array $args
     * @return void
     */
    public function __call(string $method, array $args)
    {
        return call_user_func_array([$this->dbCon, $method], $args);
    }

    /**
     * Run single prepared statements smoothly
     *
     * @param string $sql
     * @param array $args
     * @return \PDOStatement
     */
    public function query(string $sql, array $args = []): \PDOStatement
    {
        if (!is_array($args) || empty($args))
            return $this->dbCon->query($sql);

        $stmt = $this->dbCon->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }
}
