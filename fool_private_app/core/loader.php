<?php

/** 
 * Include and load all the classes automatically
 */

namespace Core;

final class Loader
{
    /**
     * Instantiates the object by given name
     *
     * @param string $modelName
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public static function model(string $modelName, array $params = [])
    {
        if (!class_exists($modelName)) {
            throw new \Exception('Unable to load Model ' . $modelName . ', because given class does not exist.');
        }
        if (empty($params)) {
            return new $modelName();
        }

        $reflection = new \ReflectionClass($modelName);
        return $reflection->newInstanceArgs($params);
    }

    /**
     * Function for spl_autoload_register() callback
     *
     * @param string $class
     * @return void
     */
    public static function autoLoadClass(string $class)
    {
        $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        if (!file_exists(ROOT_DIR . $class . '.php')) {
            return false;
        }

        require_once ROOT_DIR . $class . '.php';
    }

    /**
     * Redirects user to specified location
     *
     * @param string $loc
     * @return void
     */
    public static function redirect(string $loc)
    {
        header('Location:' . $loc);
        exit; // terminate the script after the redirection
    }
}
