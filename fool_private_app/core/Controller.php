<?php

/** 
 * Default controller
 */

namespace Core;

abstract class Controller extends Router
{
    /** 
     * All initialized models
     * @var array
     */
    private static $models = [];

    /** 
     * Default constructor for inherited controllers
     * @return void
     */
    public function __construct()
    {
    }

    /** 
     * Loads model and gives back the requested model instance
     * @param string $modelName
     * Model that has been requested (with namespace prefix)
     * @param array $params
     * All the parameters that should be passed to the given class
     * @param bool $isGlobalModel
     * @return mixed - object
     * @throws Exception
     */
    public static function loadModel(string $modelName, array $params = [], $isGlobalModel = false)
    {
        $model = Loader::model($modelName, $params);
        if ($isGlobalModel) {
            self::setGlobalModel($modelName, $model);
        }
        return $model;
    }

    /** 
     * Sets given model to be treated like global one
     * @param string $modelName
     * @param mixed $model
     * @return void
     * @throws Exception
     */
    private static function setGlobalModel(string $modelName, $model)
    {
        if (!is_object($model)) {
            throw new \Exception('Given model is not an object type.');
        }
        if (isset(self::$models[$modelName])) {
            throw new \Exception('Given model is already set.');
        }
        self::$models[$modelName] = $model;
    }

    /** 
     * Access the global model
     * @param string $modelName
     * @return mixed - object
     */
    public static function globalModel(string $modelName)
    {
        if (!isset(self::$models[$modelName])) {
            throw new \Exception('Unable to find given model ' . $modelName . ' in loaded global models list.');
        }

        return self::$models[$modelName];
    }

    /** 
     * @return \Smarty
     */
    public static function getView(): \Smarty
    {
        return self::globalModel('\Smarty');
    }
}
