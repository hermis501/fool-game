<?php

/** 
 * User roles
 * Indicates if user can reach given controller or controller's method
 */

namespace Core;

final class RoleException extends \Exception
{
}

final class Role
{
    // Possible roles
    const USER = 0; // Never check against that (it doesn't make sence to forbidden admins to do user functions)
    const SUPER_ADMIN = 1;
    const ADMIN = 2;
    const MAX_ROLE = 2; // Maximum number (of last role)

    /**
     * Role that user has
     *
     * @var integer
     */
    private $userRole;

    /**
     * Initializes given user role
     *
     * @param integer $role
     */
    public function __construct(int $role)
    {
        $this->setRole($role);
    }

    /**
     * Requires user role to be one of a given set of roles 
     *
     * @param integer $rolesAllowed
     * @return boolean
     * @throws RoleException
     */
    public function requireRole(int $rolesAllowed): bool
    {
        if ($this->checkRole($rolesAllowed)) {
            return true;
        } else {
            throw new RoleException('You have no rights to view this page.');
            return false;
        }
    }

    /**
     * Checks if at least one of the roles that are required is included
     *
     * @param integer $rolesAllowed
     * @return void
     */
    public function checkRole(int $rolesAllowed): bool
    {
        return $this->userRole & $rolesAllowed ? true : false;
    }

    /**
     * Sets or changes a role for the user
     *
     * @param integer $role
     * @return void
     * @throws Exception
     */
    public function setRole(int $role)
    {
        if ($role < 0 || $role > Role::MAX_ROLE)
            throw new \Exception('Role parameter given to User does not exist.');

        $this->userRole = $role;
    }

    /**
     * Gets user role
     *
     * @return void
     */
    public function getRole(): int
    {
        return $this->userRole;
    }
}
