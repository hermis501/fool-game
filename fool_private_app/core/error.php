<?php

/**
 * Class handles all the uncaught Exceptions and errors if needed
 */

namespace Core;

class Error
{
    /** 
     * @var object
     */
    private $smarty;

    /**
     *
     * @param string $logsDir
     * @param \Smarty $smarty
     */
    public function __construct(string $logsDir, \Smarty $smarty)
    {
        $this->smarty = $smarty;
        $this->smarty->assign('show_admin_panel', false);
        if (!file_exists($logsDir)) {
            trigger_error('Logs directory doesn\'t exist.');
        }
        $logFile = date('Y-m-d') . '.txt';
        ini_set('error_log', $logsDir . $logFile);
    }

    /**
     * Catches uncaught exceptions
     * Writes errors to the set log file
     * Sets appropriate error messages to be printed to the user
     *
     * @param object $exception
     * @return void
     */
    public function handleExceptions(object $exception)
    {
        try {
            if (get_class($exception) === 'Core\RoleException') {
                $this->smarty->assign('error_message', $exception->getMessage());
                $this->smarty->display('errors' . DIRECTORY_SEPARATOR . 'user_access_errors.tpl');
            } else {
                $message = 'Uncaught Exception: ' . get_class($exception) . PHP_EOL;
                $message .= 'Error message: ' . $exception->getMessage() . PHP_EOL;
                $message .= 'File: ' . $exception->getFile() . PHP_EOL;
                $message .= 'Line: ' . $exception->getLine() . PHP_EOL;
                $message .= 'Stack Trace: ' . $exception->getTraceAsString() . PHP_EOL;
                error_log($message);
                $this->smarty->assign('error_message', $exception->getMessage());
                $this->smarty->display('errors' . DIRECTORY_SEPARATOR . 'exception_errors.tpl');
            }
        } catch (Exception $e) {
            die('Crytical System Error: ' . $e->getMessage());
        }
    }

    public function __destruct()
    {
        unset($this->smarty);
    }
}
