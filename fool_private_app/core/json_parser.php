<?php

/**
 * Simple class to keep JSON encode / decode
 * output given JSON to the client's browser
 */

namespace Core;

class JSONParser
{
    protected $data;

    /**
     * Set's JSON data if it is given by the caller
     *
     * @param string $data
     */
    public function __construct(string $data = NULL)
    {
        $this->setData($data);
    }

    /**
     * Sets JSON data (given as a string)
     * @param mixed $data
     * array or object
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Prints JSON data to the client's browser
     *
     * @param boolean $auto_terminate
     * Indicates whether to break script execution
     * @return void
     */
    public function send($autoTerminate = false)
    {
        if (!is_array($this->data) && !is_object($this->data)) {
            throw new \Exception('JSON data must be an array or object in order to send it to the client browser HTTP Request.');
        }

        header('Content-type: application/json');
        $encodedData = json_encode($this->data);
        if ($encodedData === false) { // Impossible to encode JSON data (data is invalid)
            $encodedData = json_encode([]);
            http_response_code(500);
        }

        if ($autoTerminate) {
            die($encodedData); // Prints data to the browser and breaks script execution
        } else {
            echo $encodedData;
        }
    }

    /**
     * Parses JSON string to an array or an object respectively
     *
     * @param boolean $isArray
     * If this param is false, then it returns JSON data as an object
     * @return mixed
     */
    public function parse($isArray = false)
    {
        if (!is_string($this->data)) {
            return false;
        }

        $decodedData = json_decode($this->data, $isArray);
        if ($decodedData == null) {
            return false;
        }

        return $encodedData;
    }
}
