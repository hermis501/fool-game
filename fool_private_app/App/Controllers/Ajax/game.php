<?php

namespace App\Controllers\Ajax;

class Game extends \Core\Controller
{
    protected $json;
    protected $errors;
    protected $user;
    protected $game;

    public function __construct()
    {
        $this->json = self::loadModel('\Core\JSONParser');
        $this->errors = \Core\FormErrorCollector::instance();
        $this->user = self::globalModel('\App\Models\User');

        // Check if user is logged in
        if (!$this->user->isAuthorized()) {
            \Core\Loader::redirect('/home');
        }

        $this->game = self::loadModel('\App\Models\Game');
    }

    public function createAction() // Initiates create new table action
    {
        $gameId = $this->game->create();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => 'Table created. Wait for other players to join and then have some fun!'
        ]);
        $this->json->send();
        return true;
    }

    public function leaveAction() // wishes to leave the game
    {
        $this->game->leave();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => 'You left the table.'
        ]);
        $this->json->send();
        return true;
    }

    public function games_listAction()
    {
        $gamesList = $this->game->getGamesList();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $gamesList
        ]);
        $this->json->send();
        return true;
    }

    public function joinAction($gameId = '')
    {
        $gameId = (int) $gameId;
        $oldRequestKey = $this->game->getNewestRequestKey($gameId); // Player who joins the table do not need to see that some time ago someone created it
        $ret = \App\Models\Game::join($gameId);
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'lrkey' => $oldRequestKey, // Not need newest one, info that someone created table
            'message' => 'You joined the table.'
        ]);

        $this->json->send();
        return true;
    }

    public function get_statusAction()
    {
        $user = self::globalModel('\App\Models\User');
        $status = $this->game->getStatus();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $status,
            'userId' => $user->getId(),
            'masterId' => $this->game->getMasterId()
        ]);
        $this->json->send();
        return true;
    }

    public function waiting_for_playersAction()
    {
        $gameObj = $this->game->getPullingResults();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $gameObj
        ]);
        $this->json->send();
        return true;
    }

    public function startAction()
    {
        $this->game->start();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => 'Game started.'
        ]);

        $this->json->send();
        return true;
    }

    public function in_progressAction()
    {
        $gameObj = $this->game->getPullingResults();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $gameObj
        ]);
        $this->json->send();
        return true;
    }

    public function play_cardAction($cardCode = '')
    {
        $card = $this->game->playCard($cardCode);
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $this->user->getUsername() . ' plays ' . $card->getName()
        ]);

        $this->json->send();
        return true;
    }

    public function pass_turnAction()
    {
        $this->game->passTurn();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $this->user->getUsername() . ' passes.'
        ]);

        $this->json->send();
        return true;
    }

    public function take_homeAction()
    {
        $this->game->throwHome();
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => $this->user->getUsername() . ' takes home.'
        ]);

        $this->json->send();
        return true;
    }

    public function send_messageAction()
    {
        $message = isset($_POST['message']) ? (string) $_POST['message'] : '';
        $this->game->sendChatMessage($message);
        if ($this->errors->hasErrors()) {
            $errors = $this->errors->getErrors();
            $this->json->setData([
                'status' => 'ERROR',
                'message' => $errors[0]
            ]);
            $this->json->send();
            return true;
        }

        $this->json->setData([
            'status' => 'OK',
            'message' => 'Message sent'
        ]);

        $this->json->send();
        return true;
    }

}
