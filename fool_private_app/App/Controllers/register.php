<?php

namespace App\Controllers;

class Register extends \Core\Controller
{
    protected $user;
    protected $view;
    protected $fieldsInfo;
    private $errors;

    public function __construct()
    {
        $this->errors = \Core\FormErrorCollector::instance();
        $this->user = self::globalModel('\App\Models\User');
        if ($this->user->isAuthorized()) {
            \Core\Loader::redirect('/home');
        }
        $this->view = self::getView();
        $this->fieldsInfo = [
            'username' => ['name' => 'username', 'type' => 'str', 'min' => 3, 'max' => 30, 'pattern' => 'alphanumeric', 'value' => ''],
            'email' => ['name' => 'email address', 'type' => 'str', 'min' => 8, 'max' => 254, 'pattern' => 'email', 'value' => ''],
            'password' => ['name' => 'password', 'type' => 'str', 'min' => 6, 'max' => 70, 'pattern' => 'none', 'identical_with' => 'confirm_password', 'value' => ''],
            'confirm_password' => ['name' => 'confirm password', 'type' => 'str', 'min' => 6, 'max' => 70, 'pattern' => 'none', 'value' => '']
        ];
    }

    public function submitAction()
    {
        $fields = self::loadModel('\Core\Validation', [$this->fieldsInfo]);
        $user = self::loadModel('\App\Models\User', [\App\Models\User::ACTION_REGISTER, $fields]);
        if ($this->errors->hasErrors()) {
            $this->view->assign('errors', $this->errors->getErrors());
            $this->view->assign('fields_values', \Core\Validation::getFieldsValues($fields->getFields()));
            return 'register' . DIRECTORY_SEPARATOR . 'register_form.tpl';
        } else {
            return 'register' . DIRECTORY_SEPARATOR . 'register_form_success.tpl';
        }

        return false;
    }

    public function indexAction()
    {
        $this->view->assign('fields_values', \Core\Validation::getFieldsValues($this->fieldsInfo));
        return 'register' . DIRECTORY_SEPARATOR . 'register_form.tpl';
    }
}
