<?php

namespace App\Controllers;

class Ajax extends \Core\Controller
{
    public function __construct()
    {
    }

    private function loadAjax($controller, $method, $args)
    {
        /* ajax security, turn on at rpoduction
        $domainPrefix = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https://' : 'http://'; // https or http request
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest') { // Ajax request header isn't set, it is a fake request
            return false;
        }
        if (!isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], $domainPrefix . $_SERVER['SERVER_NAME']) !== 0) { // HTTP_REFERER is not the same site, so there is an abreach from fird-party application
            return false;
        }
End of ajax security*/

        $controller = '\App\Controllers\Ajax\\' . $controller;
        $method = \Core\Router::formatName($method) . 'Action';
        if (!method_exists($controller, $method) || !is_callable($controller, $method)) {
            return false;
        }

        $controllerObject = new $controller();
        unset($args[0]); // Remove first parameter of the caller given args (it's the same as the caller's method name without prefix)
        return call_user_func_array([$controllerObject, $method], $args);
    }

    public function gameAction($action = '')
    {
        return $this->loadAjax('game', $action, func_get_args());
    }
}
