<?php

namespace App\Controllers;

class Login extends \Core\Controller
{
    protected $user;
    protected $view;
    protected $fieldsInfo;
    private $errors;

    public function __construct()
    {
        $this->errors = \Core\FormErrorCollector::instance();
        $this->user = self::globalModel('\App\Models\User');
        if ($this->user->isAuthorized()) {
            \Core\Loader::redirect('/home');
        }

        $this->view = self::getView();
        $this->fieldsInfo = [
            'username' => ['name' => 'user name', 'type' => 'str', 'min' => 3, 'max' => 30, 'pattern' => 'alphanumeric', 'value' => ''],
            'password' => ['name' => 'password', 'type' => 'str', 'min' => 6, 'max' => 70, 'pattern' => 'none', 'value' => '']
        ];

        if (isset($_POST['remember_me']))
            $this->fieldsInfo['remember_me'] = [
                'name' => 'keep me loged in', 'type' => 'str', 'min' => 0, 'max' => 0, 'pattern' => 'none', 'value' => ''
            ];
    }

    public function submitAction()
    {
        $fields = self::loadModel('\Core\Validation', [$this->fieldsInfo]);
        $user = self::loadModel('\App\Models\User', [\App\Models\User::ACTION_LOGIN, $fields]);
        if ($this->errors->hasErrors()) {
            $this->view->assign('errors', $this->errors->getErrors());
            $this->view->assign('fields_values', \Core\Validation::getFieldsValues($fields->getFields()));
            return 'login' . DIRECTORY_SEPARATOR . 'login_form.tpl';
        } else {
            \Core\Loader::redirect('/home');
        }

        return false;
    }

    public function indexAction()
    {
        $this->view->assign('fields_values', \Core\Validation::getFieldsValues($this->fieldsInfo));
        return 'login' . DIRECTORY_SEPARATOR . 'login_form.tpl';
    }
}
