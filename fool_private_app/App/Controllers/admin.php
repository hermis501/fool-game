<?php

namespace App\Controllers;

class Admin extends \Core\Controller
{
    protected $view;

    public function __construct()
    {
        $role = self::globalModel('Core\Role');
        $role->requireRole(\Core\Role::ADMIN | \Core\Role::SUPER_ADMIN);
        $this->view = self::getView();
    }

    private function load($controller, $method, $args)
    {
        $controller = '\App\Controllers\Admin\\' . $controller;
        $method = \Core\Router::formatName($method) . 'Action';
        if (!method_exists($controller, $method) || !is_callable(array($controller, $method))) {
            return false;
        }
        $controllerObject = new $controller();
        unset($args[0]); // Remove first parameter of the caller given args (it's the same as the caller's method name without prefix)
        return call_user_func_array([$controllerObject, $method], $args);
    }

    public function indexAction()
    {
        return 'admin' . DIRECTORY_SEPARATOR . 'index.tpl';
    }
}
