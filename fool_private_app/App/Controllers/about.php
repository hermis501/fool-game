<?php

namespace App\Controllers;

class About extends \Core\Controller
{
    protected $view;

    public function __construct()
    {
        $this->view = self::getView();
    }

    public function indexAction()
    {
        return 'about.tpl';
    }
}
