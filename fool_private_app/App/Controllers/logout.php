<?php

namespace App\Controllers;

class Logout extends \Core\Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = self::globalModel('\App\Models\User');
    }

    public function indexAction()
    {
        if ($this->user->isAuthorized()) {
            $this->user->logout();
        }
        \Core\Loader::redirect('/home');
    }
}
