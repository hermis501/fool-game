<?php

namespace App\Controllers;

class Home extends \Core\Controller
{
    protected $view;
    protected $user;

    public function __construct()
    {
        $this->view = self::getView();
        $this->user = self::globalModel('\App\Models\User');
    }

    public function indexAction()
    {
        $this->view->assign('username', $this->user->getUsername());
        return 'index.tpl';
    }
}
