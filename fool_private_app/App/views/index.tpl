{* Smarty *}
{if $user_authorized}
    {assign var="js_header" value=["/js/modules/game.pull.js?v=1.61", "/js/modules/game.controls.js?v=1.61"]}
    {assign var="no_bootstrap" value=true}
    {assign var="title" value="Fool online"}
{else}
    {assign var="title" value="Fool online, game for everyone"}
{/if}
{include file="header.tpl"}
{if not $user_authorized}
    {include file="top_navigation.tpl"}
{/if}
<main class="container">
    {if not $user_authorized}
        <h1 class="my-4">Play Fool online</h1>
        <div class="jumbotron">
            <p>Play fool online for free with other players around the globe directly in your browser.</p>
            <p>Fool online is accessible for everyone (doesn&apos;t matter if you are blind, partially sighted or have regular eyesight).</p>
            <p>Just invite your friends and have some fun!</p>
        </div>
    {else}
        <div id="game-info-wrapper" class="d-none">
            <div id="trump-card"></div>
            <div id="cards-in-deck"></div>
            <div id="players"></div>
        </div>
        <div id="cards-in-play-wrapper" class="d-none"></div>
        <div id="cards-in-hand-wrapper" class="d-none">
            <h1 id="cards-in-hand-h" class="visually-hidden">Your hand</h1>
            <div id="cards-in-hand"></div>
        </div>
        <h1 id="actions-h" class="visually-hidden">Actions</h1>
        <div class="list-group" id="actions"></div>
        <div id="history-wrapper">
            <h1 id="history-h" class="visually-hidden">History</h1>
            <form>
                <textarea id="history" contenteditable="true" readonly="readonly" rows="5" cols="50" wrap="off"></textarea>
                <input type="hidden" id="lrkey" name="lrkey" value="" />
            </form>
        </div>
        <div id="chat-wrapper" class="d-none">
            <h1 id="chat-h" class="visually-hidden">Chat</h1>
            <form id="chat-form">
                <textarea id="chat-content" rows="5" cols="50" wrap="off" contenteditable="true" readonly="readonly" placeholder="Chat"></textarea>
                <input type="text" id="chat-message" placeholder="Say something..." maxlength="100" autocomplete="off">
                <input type="submit" id="chat-send-btn" value="Send" />

            </form>
        </div>
        <div id="screen-reader-only" class="visually-hidden" aria-live="assertive" aria-relevant="all" aria-atomic="true"></div>
    {/if}
</main>
{include file="footer.tpl"}