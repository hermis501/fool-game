{* Smarty *}
{if not isset($no_bootstrap)}
<footer class="page-footer font-small pt-4">
    <div class="footer-copyright text-center py-3">&copy; <a href="/about">Fool online team</a>, {display_current_year} all rights reserved.</div>
</footer>
{/if}
</body>
</html>