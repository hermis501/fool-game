{* smarty *}
{assign var="title" value="Login | Fool online"}
{include file="header.tpl"}
{include file="top_navigation.tpl"}
<main class="container">
    <div class="card my-4">
        <h1 class="card-header">Login to your account</h1>
        <div class="card-body">
            {if isset($errors)}
                <div class="alert alert-danger" role="alert">
                    <ul class="list-group">
                        {foreach from=$errors item=error}
                            <li class="list-group-item">{$error}</li>
                        {/foreach}
                    </ul>
                </div>
            {/if}

            <form id="login-form" action="/login/submit" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{$fields_values.username|escape}" maxlength="30" autocomplete="off" required="required" autofocus="autofocus" />
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="" required="required" />
                </div>

                <div class="checkbox">
                    <label><input type="checkbox" id="remember-me" name="remember_me" value="remember-me" {if isset($fields_values.remember_me)}checked="checked" {/if} />Keep me logged in</label>
                </div>

                <input type="submit" class="btn btn-primary btn-sm" value="Login" />
            </form>
        </div>
    </div>
</main>
{include file="footer.tpl"}