{* Smarty *}
{assign var="title" value="Account created | Fool online"}
{include file="header.tpl"}
{include file="top_navigation.tpl"}
<main class="container">
    <h1 class="my-4">Registration</h1>
    <div class="alert alert-success">Congratulations, You have successfully registered to our game. We hope you will enjoy what we created.</div>
</main>
{include file="footer.tpl"}