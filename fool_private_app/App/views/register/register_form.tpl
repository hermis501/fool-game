{* Smarty *}
{assign var="title" value="Registration | Fool online"}
{assign var="js_header" value="/js/modules/register.js"}
{include file="header.tpl"}
{include file="top_navigation.tpl"}
<main class="container">
    <div class="card">
        <h1 class="card-header my-4">Create account</h1>
        <div class="card-body">
            {if isset($errors)}
                <div class="alert alert-danger" role="alert">
                    <ul class="list-group">
                        {foreach from=$errors item=error}
                            <li class="list-group-item">{$error}</li>
                        {/foreach}
                    </ul>
                </div>
            {/if}

            <form id="register-form" action="/register/submit" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{$fields_values.username|escape}" maxlength="30" required="required" autofocus="autofocus" />
                    <div class="err-msg"></div>
                </div>

                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" value="{$fields_values.email|escape}" maxlength="254" required="required" />
                    <div class="err-msg"></div>
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="" required="required" />
                    <div class="err-msg"></div>
                </div>

                <div class="form-group">
                    <label for="confirm-password">Confirm password</label>
                    <input type="password" class="form-control" id="confirm-password" name="confirm_password" value="" required="required" />
                    <div class="err-msg"></div>
                </div>

                <input type="submit" class="btn btn-primary btn-sm" value="Create account" />
            </form>
        </div>
    </div>
</main>
{include file="footer.tpl"}