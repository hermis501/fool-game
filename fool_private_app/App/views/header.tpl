{* Smarty *}
{if not isset($title)}
    {assign var="title" value="Fool online"}
{/if}
<!DOCTYPE html>
<html lang="en-us">

<head>
    <meta charset="utf-8" />
    <base href="/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, shrink-to-fit=no" />
    <!--[if lt IE 9]>
<script type="text/javascript" src="/js/html5shiv/html5shiv.js"></script>
<![endif]-->
    <script src="/js/jquery.min.js"></script>
    {if not isset($no_bootstrap)}
        <link rel="stylesheet" type="text/css" href="/css/bootstrap/bootstrap.min.css" media="screen" />
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap/bootstrap.min.js"></script>
    {/if}
    <link rel="stylesheet" type="text/css" href="/css/custom/default.css?v=1.2" media="screen" />
    <link rel="stylesheet" type="text/css" href="/css/fontawesome/css/all.min.css" media="screen" />
    {if isset($css_header)}
    {foreach from=$css_header item="header_part"}
    <link rel="stylesheet" type="text/css" href="{$header_part}" media="screen" />
    {/foreach}
    {/if}
    {if isset($js_header)}
    {foreach from=$js_header item="header_part"}
    <script src="{$header_part}"></script>
    {/foreach}
    {/if}
    <link rel="shortcut icon" type="image/png" href="/favicon.ico" />
    <title>{$title}</title>
</head>

<body>