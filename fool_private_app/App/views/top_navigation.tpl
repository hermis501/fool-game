{* Smarty *}
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
    <div class="container-fluid">
        <header>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsible-navbar" aria-controls="collapsible-navbar" aria-expanded="false" aria-label="Expandable navigation">
                <span class="navbar-toggler-icon" aria-hidden="true"></span>
            </button>
            <a class="navbar-brand" href="/home">Fool online</a>
        </header>

        <div class="collapse navbar-collapse" id="collapsible-navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="/home"><i class="fas fa-home" aria-hidden="true"></i> Home</a></li>
                <li class="nav-item"><a class="nav-link" href="/about"><i class="fas fa-envelope" aria-hidden="true"></i> How to play</a></li>
                {if isset($user_authorized) and $user_authorized}
                {if $show_admin_panel}
                <li class="nav-item"><a class="nav-link" href="/admin"><i class="fas fa-user-secret" aria-hidden="true"></i> Admin panel</a></li>
                {/if}
                <li class="nav-item"><a class="nav-link" href="/logout"><i class="fas fa-sign-out-alt" aria-hidden="true"></i> Sign-out</a></li>
                {else}
                <li class="nav-item"><a class="nav-link" href="/login"><i class="fas fa-sign-in-alt" aria-hidden="true"></i> Login</a></li>
                <li class="nav-item"><a class="nav-link" href="/register"><i class="fas fa-lock" aria-hidden="true"></i> Create your acount</a></li>
                {/if}
            </ul>
        </div>
    </div>
</nav>