<?php

namespace App\Models;

/**
 * Collects and manipulates the player data in the game
 * Handles cards in the hand
 */
class Player
{
    /**
     *
     * @var \Core\MyPDO
     */
    protected $db;

    /**
     *
     * @var \Core\FormErrorCollector
     */
    protected $errors;

    /**
     * Instance of global model user
     *
     * @var User
     */
    protected $user;

    /**
     * ID of the current player
     *
     * @var integer
     */
    protected $id;

    /**
     * Game that Player belongs to ID	
     *
     * @var integer
     */
    protected $gameId;

    /**
     * Player username
     *
     * @var string
     */
    protected $username;

    /**
     * Cards that are currently in player's hand
     *
     * @var array
     */
    protected $cardsInHand = [];

    /**
     * Player turn number (to reflect the game order)
     *
     * @var integer
     */
    protected $turnIndex = null;

    /**
     * If player passed during his last turn
     *
     * @var boolean
     */
    protected $passed = false;

    /**
     * Initialises Player object
     *
     * @param int $gameId
     * @param integer $userId
     * @param string $username
     */
    public function __construct(int $gameId, int $userId, string $username)
    {
        $this->db = \Core\MyPDO::instance();
        $this->errors = \Core\FormErrorCollector::instance();
        $this->user = \Core\Controller::globalModel('\App\Models\User');
        $this->gameId = $gameId;
        $this->id = $userId;
        $this->username = $username;

        try {
            $this->getData();
        } catch (\Exception $e) {
            $this->errors->add($e->getMessage());
        }
    }

    /**
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Adds given cards to the hand
     * Saves hand in the database
     *
     * @param array $cards
     * Card objects
     * @return void
     */
    public function addCards(array $cardsToAdd)
    {
        if (empty($cardsToAdd)) {
            return;
        }

        $this->cardsInHand = array_merge($this->cardsInHand, $cardsToAdd);
        $cardsToDB = [];
        foreach ($this->cardsInHand as $card) {
            $cardCode = $card->getCode();
            $cardsToDB[] = $cardCode;
        }
        $cards = implode(',', $cardsToDB);
        $query = "UPDATE game_player SET cards = :cards";
        $query .= " WHERE user_id = :userId LIMIT 1";
        $this->db->query($query, [':userId' => $this->id, ':cards' => $cards]);
    }

    /**
     * Gets player cards from database
     * Retrieves the number of turn index to determine when will player's turn to go will be
     * Also info if player's last turn was passing or not
     *
     * @return void
     */
    protected function getData()
    {
        $query = "SELECT cards, turn_index, passed FROM game_player";
        $query .= " WHERE user_id = :userId LIMIT 1";
        $data = $this->db->query($query, [':userId' => $this->id])->fetch();
        if (!$data) {
            $this->cardsInHand = [];
            $this->turnIndex = null;
            $this->passed = false;
            return;
        }

        $this->turnIndex = $data['turn_index'];
        $this->passed = (bool) $data['passed'];
        if (empty($data['cards'])) {
            $this->cardsInHand = [];
            return;
        }

        // Convert cards codes to their corresponding objects
        $cards = explode(',', $data['cards']);
        $playerCards = [];
        foreach ($cards as $card) {
            $playerCards[$card] = new Card($card);
        }

        $this->cardsInHand = $playerCards;
    }

    /**
     * Gets all the cards in player's hand
     *
     * @return array
     * Cards with their codes as keys and names as values
     */
    public function getCardsInHand(): array
    {
        $cards = [];
        foreach ($this->cardsInHand as $card) {
            $cards[$card->getCode()] = $card->getName();
        }

        return $cards;
    }

    /**
     * Gets index / number of a turn which is assigned to the player
     *
     * @return integer
     */
    public function getTurnIndex(): int
    {
        return $this->turnIndex;
    }

    /**
     * Returns info if player's last turn was pass or not
     *
     * @return boolean
     */
    public function getPassed(): bool
    {
        return $this->passed;
    }

    /**
     * Checks if player owns the card by given code or not
     *
     * @param string $cardCode
     * @return boolean
     */
    public function hasCard(string $cardCode): bool
    {
        if (isset($this->cardsInHand[$cardCode])) {
            return true;
        }

        return false;
    }

    /**
     * Removes given card from hand
     * and saves changes to the database
     *
     * @param string $cardCodeToRemove
     * Which card to remove from hand
     * @return bool
     */
    public function removeCardFromHand(string $cardCodeToRemove): bool
    {
        if (!isset($this->cardsInHand[$cardCodeToRemove])) {
            return false;
        }
        unset($this->cardsInHand[$cardCodeToRemove]);

        $playerCards = [];
        if (!empty($this->cardsInHand)) {
            foreach ($this->cardsInHand as $card) {
                $playerCards[] = $card->getCode();
            }
        }

        $playerCards = implode(',', $playerCards);
        $query = "UPDATE game_player SET cards = :playerCards";
        $query .= " WHERE user_id = :userId LIMIT 1";
        $this->db->query($query, [':playerCards' => $playerCards, ':userId' => $this->id]);
        return true;
    }

    /**
     * Gets all the players in game determined by game ID
     * Counts players cards in hands as well
     *
     * @param int $gameId
     * @return array
     * Empty if there are no players
     * Otherwise returns players ID's, usernames and amounts of cards in hands
     */
    public static function getGamePlayers(int $gameId): array
    {
        $db = \Core\MyPDO::instance();
        $errors = \Core\FormErrorCollector::instance();
        $query = "SELECT gp.user_id, gp.cards, u.username FROM game_player AS gp";
        $query .= " INNER JOIN user AS u";
        $query .= " ON gp.user_id = u.id";
        $query .= " WHERE gp.game_id = :gameId ORDER BY gp.turn_index";
        $players = $db->query($query, [':gameId' => $gameId])->fetchAll();
        if (!$players) {
            return [];
        }

        foreach ($players as &$player) {
            $playerCards = explode(',', $player['cards']);
            if ($playerCards[0] === '') {
                $playerCards = [];
            }
            $player['cards_in_hand'] = count($playerCards);
            unset($player['cards']);
        }

        return $players;
    }

    /**
     * Gets cards hands of all players that belong to current game
     *
     * @param integer $gameId
     * @return array
     */
    public static function getHands(int $gameId): array
    {
        $db = \Core\MyPDO::instance();

        try {
            $query = "SELECT cards FROM game_player";
            $query .= " WHERE game_id = :gameId";
            $query .= " ORDER BY turn_index ASC";
            $hands = $db->query($query, [':gameId' => $gameId])->fetchAll();
            if (!$hands) {
                throw new \Exception('Game cannot be played without cards in players hands.');
            }

            return $hands;
        } catch (\Exception $e) {
            $errors->add($e->getMessage());
            throw $e;
            return [];
        }
    }

    /**
     * Lets change player turn index (needed for starting game properly)
     *
     * @param integer $index
     * @return void
     */
    public function setTurnIndex(int $index)
    {
        $this->turnIndex = $index;
    }
}
