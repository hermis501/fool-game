<?php

namespace App\Models;

/**
 * Forms deck of the cards
 * Lets manipulate them
 */
class Deck
{
    /**
     *
     * @var \Core\MyPDO
     */
    protected $db;

    /**
     * Game which this deck belongs to ID
     *
     * @var integer
     */
    protected $gameId;

    /**
     * Cards in deck
     *
     * @var array
     */
    protected $cards = [];

    /**
     * Keeps trump card of this deck
     *
     * @var Card
     */
    protected $trumpCard;

    /**
     * Generates a new shuffled deck if game has no deck yet and assigns it to the current game by it's ID,
     * Determines trump card
     * Otherwise gets already shuffled deck from the database with trump card determined
     *
     * @param integer $gameId
     */
    public function __construct(int $gameId)
    {
        try {
            $this->db = \Core\MyPDO::instance();

            if (Game::gameExists($gameId, false) < 1) {
                throw new \Exception('Non-existing game ID has been passed.');
            }
            $this->gameId = $gameId;

            $deck = $this->getCards();
            if ($deck === false) { // Has no formed deck yet
                $this->build();
                $this->shuffle();
                $this->save(true); // Deck does not exist in the game, so create it
            } else { // Deck exists
                $this->trumpCard = new Card($deck['trump_card'], true); // true for trump card
                if (empty($deck['cards'])) { // There are no cards left in deck
                    $this->cards = [];
                } else { // Make cards as objects to reflect the deck, because there are some cards in it
                    $cards = [];
                    $i = 0;
                    foreach ($deck['cards'] as $card) {
                        $cards[$i] = new Card($card);
                        if ($this->trumpCard->getSuit() === $cards[$i]->getSuit()) { // Card should be marked as trump
                            $cards[$i]->setTrump(true);
                        }
                        ++$i;
                    }
                    $this->cards = $cards;
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Builds non-shuffled deck from all the possible cards
     *
     * @return void
     */
    protected function build()
    {
        $cards = [];
        foreach (Card::SUITS as $suitCode => $suit) {
            foreach (Card::RANKS as $rankCode => $rank) {
                $cards[] = new Card($rankCode . $suitCode);
            }
        }
        $this->cards = $cards;
    }

    /**
     * Shuffles the deck
     *
     * @return void
     */
    protected function shuffle()
    {
        $cardsTotal = count($this->cards);
        $cards = $this->cards;
        for ($i = $cardsTotal - 1; $i >= 1; $i--) {
            $randomCardIndex = mt_rand(0, $i);
            $tempCard = $cards[$randomCardIndex];
            $cards[$randomCardIndex] = $cards[$i];
            $cards[$i] = $tempCard;
        }
        $this->cards = $cards;
        $this->trumpCard = $this->cards[$cardsTotal - 1]; // Determines trump card as the last card in deck
    }

    /**
     * Draws given amount of cards from the deck
     * If there are no more cards left, it returns an empty array
     *
     * @param integer $cardsToDraw
     * @return array
     */
    public function drawCards(int $cardsToDraw): array
    {
        if ($cardsToDraw < 1) {
            throw new \Exception('Cannot draw 0 or negative amount of cards.');
        }

        $cardsTotal = count($this->cards);
        if ($cardsTotal < 1) {
            return [];
        }

        if ($cardsToDraw > $cardsTotal) {
            $cardsToDraw = $cardsTotal;
        }

        $cardsDrawn = [];
        $i = 0;
        foreach ($this->cards as $key => $card) {
            if ($i >= $cardsToDraw) {
                break; // Enough cards to draw
            }

            $cardsDrawn[] = clone ($card); // Make a copy of the card by cloning and not referencing to the same object
            unset($this->cards[$key]); // Remove drawn card from the deck
            ++$i;
        }

        return $cardsDrawn;
    }

    /**
     * Creates deck for the game if needed,
     * Otherwise updates already existing deck with the current cards
     * Cards are saved in CSV format (cards codes separated by ',' (without ''))
     *
     * @param boolean $needCreate
     * Whether to insert deck or update it
     * @return integer
     * Amount of cards that left in the deck
     */
    public function save(bool $needCreate = false): int
    {
        $cards = $this->cards;
        $cardsTotal = count($cards);
        $deckToSave = [];
        foreach ($cards as $card) {
            $deckToSave[] = $card->getCode();
        }
        $deck = implode(',', $deckToSave);

        if ($needCreate) {
            $query = "INSERT INTO game_deck (game_id, cards, trump_card)";
            $query .= " VALUES (:gameId, :cards, :trumpCard)";
            $this->db->query($query, [':gameId' => $this->gameId, ':cards' => $deck, ':trumpCard' => $this->trumpCard->getCode()]);
        } else { // Update deck
            $query = "UPDATE game_deck SET cards = :cards";
            $query .= " WHERE game_id = :gameId LIMIT 1";
            $this->db->query($query, [':gameId' => $this->gameId, ':cards' => $deck]);
        }

        return $cardsTotal;
    }

    /**
     * Removes deck from the game
     *
     * @return void
     */
    public function remove()
    {
        $query = "DELETE FROM game_deck";
        $query .= " WHERE game_id = :gameId LIMIT 1";
        $this->db->query($query, [':gameId' => $this->gameId]);
    }

    /**
     * Gets codes of cards and trump card of the deck which are in database
     *
     * @return mixed
     * If deck does not exist in the game yet, method returns boolean false
     * If there are no more cards in the deck, $returnResult['cards'] will be an empty string
     * Otherwise $returnResult['cards'] contains array of the cards codes
     * $returnResult['trump_card'] holds the code of the trump card in the deck
     */
    protected function getCards()
    {
        $query = "SELECT cards, trump_card FROM game_deck";
        $query .= " WHERE game_id = :gameId LIMIT 1";
        $deck = $this->db->query($query, [':gameId' => $this->gameId])->fetch();
        if (!$deck) {
            return false;
        }

        $deck['cards'] = explode(',', $deck['cards']);
        if (count($deck['cards']) == 1 && $deck['cards'][0] == '') { // Empty deck
            $deck['cards'] = '';
        }

        return $deck;
    }

    /**
     *
     * @return string
     */
    public function getTrump()
    {
        return $this->trumpCard;
    }

    /**
     *
     * @return integer
     */
    public function countCards(): int
    {
        return count($this->cards);
    }
}
