<?php

namespace App\Models;

class User
{
    // Actions for a constructor to invoke appropriate action
    const ACTION_REGISTER = 1; // Need to register a new user
    const ACTION_LOGIN = 2; // Need to authenticate user that tries to login
    const ACTION_AUTHENTICATE = 3; // Action to check if user is logged-in

    protected $db;
    protected $data;
    private $authorized = false;
    private $id;
    private $username;
    private $role;
    private $errors;

    public function __construct($action, \Core\Validation $fields = null)
    {
        $this->db = \Core\MyPDO::instance();
        $this->errors = \Core\FormErrorCollector::instance();

        switch ($action) {
            case self::ACTION_AUTHENTICATE:
                $this->authorize();
                break;
            case self::ACTION_LOGIN:
                if (!isset($fields))
                    throw new \Exception('Fields parameter is missing, so system cannot initiate login process.');

                $this->login($fields);
                break;
            case self::ACTION_REGISTER:
                if (!isset($fields))
                    throw new \Exception('Fields parameter is missing, so system cannot initiate registration process.');

                $this->register($fields);
                break;
            default:
                throw new \Exception('Constructor requires to pick one of the appropriate actions (picked from defined constants).');
        }
    }

    public function __destruct()
    {
        $this->db = null;
    }

    // setters
    private function setId($id)
    {
        $this->id = $id;
    }

    private function setUsername($username)
    {
        $this->username = $username;
    }

    private function setRole($role)
    {
        $this->role = $role;
    }

    // Geters
    public function getUsername()
    {
        return isset($this->username) ? $this->username : '';
    }

    public function getId()
    {
        return isset($this->id) ? $this->id : 0;
    }

    public function isAuthorized()
    {
        return $this->authorized;
    }

    public function getRole()
    {
        return isset($this->role) ? $this->role : \Core\Role::USER;
    }

    public static function getIdByUsername($username)
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT id FROM user WHERE username = :username LIMIT 1";
        $data = $db->query($query, [':username' => $username])->fetch();
        if (!$data) // No such user
            return false;

        return $data['id'];
    }

    // Checking of existance methods
    public static function idExists($id)
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT id FROM user WHERE id = :user_id LIMIT 1";
        $data = $db->query($query, [':id' => $id])->fetchColumn();
        if (!$data)
            return false; // There is no such ID in DB

        return true; // ID has been found
    }

    public static function usernameExists($username)
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT username FROM user WHERE username = :username LIMIT 1";
        $data = $db->query($query, [':username' => $username])->fetchColumn();
        if (!$data)
            return false; // Username does not exist

        return true;
    }

    public static function emailExists($email)
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT email_addr FROM user WHERE email_addr = :email LIMIT 1";
        $data = $db->query($query, [':email' => $email])->fetchColumn();
        if (!$data)
            return false; // No such email address

        return true;
    }

    // Create account
    private function register(\Core\Validation $fields)
    {
        if ($this->errors->hasErrors()) {
            return false;
        }
        if (self::usernameExists($fields->getFieldValue('username'))) {
            $this->errors->add('Such username is already taken.');
        }
        if (self::emailExists($fields->getFieldValue('email'))) {
            $this->errors->add('Such Email address is already taken.');
        }
        if ($this->errors->hasErrors()) {
            return false;
        }
        $user_data = $fields->getFields();
        $user_data['password']['value'] = password_hash($user_data['password']['value'], PASSWORD_DEFAULT);
        try {
            $query = "INSERT INTO user ";
            $query .= "(username, password, registration_datetime, email_addr, user_role) ";
            $query .= "VALUES (:username, :password, NOW(), :email, :user_role)";
            $bind = array(
                ':username' => $user_data['username']['value'],
                ':password' => $user_data['password']['value'],
                ':email' => $user_data['email']['value'],
                ':user_role' => \Core\Role::USER
            );
            $this->db->query($query, $bind);
            return true;
        } catch (\PDOException $e) {
            $this->errors->add('Unable to create account. Please try again in few minutes.');
            return false;
        }
    }

    // Login methods
    private function login(\Core\Validation $fields)
    {
        if ($this->errors->hasErrors()) {
            return false;
        }
        if (!$this->checkCredentials($fields->getFieldValue('username'), $fields->getFieldValue('password'))) {
            $this->errors->add('Wrong credentials.');
            return false;
        }
        $token = self::generateAuthorizationToken();
        if ($token == false) {
            $this->errors->add('System unable to identify your account.');
            return false;
        }
        if ($fields->getFieldValue('remember_me') !== false) {
            $this->grantAccess($token, true); // Set all the authentication cookies and session with remember me
        } else {
            $this->grantAccess($token); // Set all the authentication cookies and session
        }
        return true; // Access granted
    }

    private function checkCredentials($username, $password)
    {
        $query = "SELECT id, username, password FROM user WHERE username = :username LIMIT 1";
        $data = $this->db->query($query, [':username' => $username])->fetch();
        if (!$data) // No such user
            return false;

        if (!password_verify($password, $data['password'])) // Given password and hashed password in the DB does not match
            return false;

        $this->setId($data['id']);
        return true; // User authorized, access not granted yet
    }

    public static function generateAuthorizationToken()
    { // Returns 55 chr length string which is combined with user Browser and IP, encripted with md5 and preceded with uniqid()
        $remote_browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        if ($remote_browser === '') {
            error_log('Token cannot be generated without Browser.');
            return false; // Token cannot be generated without IP or Browser
        }
        $unique = uniqid('', true); // More entropy to make it 23 chars length instead of 13
        $token = md5($remote_browser) . $unique; // 32 + 23 == 55
        return $token;
    }

    private function grantAccess($token, $remember_me = false)
    { // Sets or updates authorization Cookies and session
        $session_params = session_get_cookie_params();
        setcookie(\Core\Session::AUTHORIZATION_COOKIE_NAME, $token, time() + \Core\Session::getSessionCookieLifetime(), $session_params['path'], $session_params['domain'], \Core\Session::SECURE, \Core\Session::HTTP_ONLY);
        $_SESSION[\Core\Session::AUTHORIZATION_COOKIE_NAME] = $token;
        if (!isset($_SESSION[\Core\Session::USER_SESSION_NAME])) {
            $_SESSION[\Core\Session::USER_SESSION_NAME] = $this->getId();
        }
        if ($remember_me || isset($_COOKIE[\Core\Session::REMEMBER_ME_COOKIE_NAME])) {
            setcookie(\Core\Session::REMEMBER_ME_COOKIE_NAME, 'remember-me', time() + \Core\Session::getSessionCookieLifetime(), $session_params['path'], $session_params['domain'], \Core\Session::SECURE, \Core\Session::HTTP_ONLY);
        }
        return true;
    }

    // Method for checking user authorization
    private function authorize()
    { // Checks if user is logged-in. If it is, it set's user's id, role and other data
        if (!isset($_COOKIE[\Core\Session::AUTHORIZATION_COOKIE_NAME]) || !isset($_COOKIE[\Core\Session::SESSION_NAME])) { // If AUTHORIZATION_USER_TOKEN or PHPSESSID is not set, user isn't logged-in
            return false;
        }
        if (!isset($_SESSION[\Core\Session::AUTHORIZATION_COOKIE_NAME]) || !isset($_SESSION[\Core\Session::USER_SESSION_NAME])) { // Sessions aren't properly set, user doesn't logged-in
            $this->logout(); // Remove cookies and existed login sessions
            return false;
        }
        if ($_COOKIE[\Core\Session::AUTHORIZATION_COOKIE_NAME] !== $_SESSION[\Core\Session::AUTHORIZATION_COOKIE_NAME]) { // Tokens inDB and in Cookie do not match, there is a fake User
            $this->logout();
            return false;
        }
        $token = self::generateAuthorizationToken(); // Generate token from remote User data
        if (mb_substr($token, 0, 32) !== mb_substr($_SESSION[\Core\Session::AUTHORIZATION_COOKIE_NAME], 0, 32)) { // Remote User's data is not the same as Encripted in DB, user browser changed or it's an attempt to access to other user's data. First 32 characters of token represents Remote User info
            $this->logout();
            return false;
        }

        $user_id = $_SESSION[\Core\Session::USER_SESSION_NAME];
        $query = "SELECT username, user_role FROM user WHERE id = :id  LIMIT 1";
        $user_info = $this->db->query($query, [':id' => $user_id])->fetch();
        if (!$user_info) { // Such user does not exist anymore
            $this->logout();
            return false;
        }

        $this->setId($user_id);
        $this->setUsername($user_info['username']);
        $this->setRole($user_info['user_role']);
        $this->grantAccess($_SESSION[\Core\Session::AUTHORIZATION_COOKIE_NAME]); // Update cookies, session and token with it
        $this->authorized = true;
        return true; // Access granted, User confirmed
    }

    public function logout()
    {
        $session_params = session_get_cookie_params();
        setcookie(\Core\Session::AUTHORIZATION_COOKIE_NAME, '', time() - \Core\Session::getSessionCookieLifetime(), $session_params['path'], $session_params['domain'], \Core\Session::SECURE, \Core\Session::HTTP_ONLY);
        setcookie(\Core\Session::SESSION_NAME, '', time() - \Core\Session::getSessionCookieLifetime(), $session_params['path'], $session_params['domain'], \Core\Session::SECURE, \Core\Session::HTTP_ONLY);
        setcookie(\Core\Session::REMEMBER_ME_COOKIE_NAME, '', time() - \Core\Session::REMEMBER_ME_COOKIE_LIFETIME, $session_params['path'], $session_params['domain'], \Core\Session::SECURE, \Core\Session::HTTP_ONLY);
        unset($_SESSION[\Core\Session::AUTHORIZATION_COOKIE_NAME]);
        unset($_SESSION[\Core\Session::USER_SESSION_NAME]);
        session_destroy();
        $this->authorized = false;
    }
}
