<?php

namespace App\Models;

/**
 * Class reflects the cards that are used in this game
 */
class Card
{
    /**
     * Ranks of the possible cards
     * Array keys are the codes of the cards (combinations that are kept in DB)
     * Then first value of the 2D array is the rank, and the second value is the score / power that card has in this game
     * Pay attention that 10th card marked as 1, but means 10
     */
    const RANKS = [
        '2' => ['2', 2],
        '3' => ['3', 3],
        '4' => ['4', 4],
        '5' => ['5', 5],
        '6' => ['6', 6],
        '7' => ['7', 7],
        '8' => ['8', 8],
        '9' => ['9', 9],
        '1' => ['10', 10],
        'J' => ['Jack', 11],
        'Q' => ['Queen', 12],
        'K' => ['King', 13],
        'A' => ['Ace', 14]
    ];

    /**
     * Suit codes and their corresponding names
     */
    const SUITS = [
        'C' => 'clubs',
        'D' => 'diamonds',
        'H' => 'hearts',
        'S' => 'spades'
    ];

    /**
     * Remembers if card is trump or not
     *
     * @var boolean
     */
    protected $isCardTrump = false;

    /**
     * Where the actual images of the cards are kept
     */
    protected const IMAGES_PATH = '/images/cards/';

    /**
     * Extension to be used for the images
     */
    protected const IMAGES_EXTENSION = '.png';

    /**
     * Rank of the card
     *
     * @var string
     */
    protected $rank;

    /**
     * Suit of the card
     *
     * @var string
     */
    protected $suit;

    /**
     * Score of the card (it's power)
     *
     * @var integer
     */
    protected $score;

    /**
     * Complete card name that can be use to warn users which card has been played
     *
     * @var string
     */
    protected $name;

    /**
     * Complete path to the card image
     *
     * @var string
     */
    protected $imagePath;

    /**
     * Loads card with it's properties by using given card code
     *
     * @param string $cardCode
     * This code should consist of 2 characters
     * First character reflects the rank
     * And second the suit of the card
     * @param boolean $isTrump
     * If card is trump, it wil get greater score
     */
    public function __construct(string $cardCode, bool $isTrump = false)
    {
        // Check if code consists only of 2 characters
        if (strlen($cardCode) !== 2) {
            throw new \Exception('Card code must consist of 2 characters.');
        }

        // Check if such code can actually exist as a card
        if (!array_key_exists($cardCode[0], self::RANKS) || !array_key_exists($cardCode[1], self::SUITS)) {
            throw new \Exception('Card code must consist of valid rank and suit.');
        }

        // Properly form the card
        $this->rank = $cardCode[0];
        $this->suit = $cardCode[1];
        $this->name = self::RANKS[$this->rank][0] . ' of ' . SELF::SUITS[$this->suit];
        $this->imagePath = self::IMAGES_PATH . self::SUITS[$this->suit] . '/' . $this->rank . self::IMAGES_EXTENSION;
        $this->setTrump($isTrump);
    }

    /**
     *
     * @return string
     */
    public function getRank(): string
    {
        return $this->rank;
    }

    /**
     *
     * @return string
     */
    public function getSuit(): string
    {
        return $this->suit;
    }

    /**
     * Gets code of the card (in combination of rank and suit)
     *
     * @return string
     */
public function getCode(): string
{
return $this->getRank() .  $this->getSuit();
}

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @return integer
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     *
     * @return string
     */
    public function getImagePath(): string
    {
        return $this->imagePath;
    }

    /**
     *
     * @return boolean
     */
    public function isTrump(): bool
    {
        return $this->isCardTrump;
    }

    /**
     * Sets or changes trump state, that determines if card is a trump or not
     * If card is trump, it gets more score
     * If trump state for the card is reset (set once more), score is recalculated
     *
     * @param boolean $isTrump
     * @return void
     */
    public function setTrump(bool $isTrump)
    {
        $this->isCardTrump = $isTrump;
        $this->score = self::RANKS[$this->rank][1];

        if ($isTrump) {
            $this->score += 13; // That many cards can max be in one suit, so non-trump cards never be more powerful than trump ones
        }
    }
}
