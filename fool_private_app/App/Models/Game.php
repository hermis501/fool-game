<?php

namespace App\Models;

/**
 * Handles all the game process
 */
class Game
{
    /**
     *
     * @var \Core\MyPDO
     */
    protected $db;

    /**
     *
     * @var \Core\FormErrorCollector
     */
    protected $errors;

    /**
     * Instance of global model user
     *
     * @var User
     */
    protected $user;

    /**
     * Game ID 
     *
     * @var int
     */
    protected $id;

    /**
     * Status of the current game
     * For example - 'in_progress', 'waiting_for_players'...
     *
     * @var string
     */
    protected $status;

    /**
     * Saves who is the master of the current table
     * 
     * @var integer
     */
    protected $masterId;

    /**
     *
     * @var integer
     */
    protected $masterUsername;

    /**
     * Index of the turn that is currently in the game
     * It is a number assigned to the player
     *
     * @var integer
     */
    protected $currentTurn;

    /**
     * ID of current attacker
     *
     * @var integer
     */
    protected $currentAttackerId;

    /**
     * ID of current defender
     *
     * @var integer
     */
    protected $currentDefenderId;

    /**
     * How many players can be at the same table at one time
     */
    const PLAYERS_PER_GAME = 6; // Normally should not be changed

    /**
     * Deck which is used in current game
     *
     * @var Deck
     */
    protected $deck;

    /**
     * Trump card on the table
     *
     * @var Card
     */
    protected $trumpCard;

    /**
     * Cards that are currently on the table
     *
     * @var array
     */
    private $cardsInPlay = [];

    /**
     * Players who belong to the current game
     *
     * @var array
     * Player objects
     */
    protected $players = [];

    /**
     * Holds info about the game order (who moves after whoom)
     *
     * @var array
     */
    protected $playOrder = [];

    /**
     * Marks if defender is currently taking cards home, so others can throw suitable cards him to take in addition
     *
     * @var boolean
     */
    protected $defenderTakingHome = false;

    public function __construct()
    {
        $this->db = \Core\MyPDO::instance();
        $this->errors = \Core\FormErrorCollector::instance();
        $this->user = \Core\Controller::globalModel('\App\Models\User');
        $this->id = self::userBelongsToGame($this->user->getId());
        if ($this->id > 0) { // User is in the game
            $this->deck = new Deck($this->id);
            $this->trumpCard = $this->deck->getTrump();
            $players = Player::getGamePlayers($this->id);
            if (empty($players)) {
                throw new \Exception('Game error: there are no players in the Game, although data determines otherwise. Please report this error to administrators.');
            }

            // Initialize Player objects
            foreach ($players as $key => $player) {
                $this->players[$players[$key]['user_id']] = new Player($this->id, $players[$key]['user_id'], $players[$key]['username']);
            }
        }
    }

    /**
     * Creates new game and makes current user as master
     *
     * @return integer
     * New game ID
     * If return 0, it indicates an error
     */
    public function create(): int
    {
        if ($this->id > 0) {
            $this->errors->add('You are already at the table.');
            return 0;
        }

        try {
            $this->db->beginTransaction();

            $query = "INSERT INTO game (user_id, status)";
            $query .= " VALUES(:userId, 'waiting_for_players')";
            $stmt = $this->db->query($query, [':userId' => $this->user->getId()]);
            $gameId = $this->db->lastInsertId('id');
            if ($gameId == 0) {
                throw new \PDOException('Unable to get created game ID.');
            }
            $query = "INSERT INTO game_player (game_id, user_id)";
            $query .= " VALUES(:gameId, :userId)";
            $stmt = $this->db->query($query, [':gameId' => $gameId, ':userId' => $this->user->getId()]);
            $query = "INSERT INTO game_log (game_id, user_id, action)";
            $query .= " VALUES(:gameId, :userId, 'created_game')";
            $stmt = $this->db->query($query, [':gameId' => $gameId, ':userId' => $this->user->getId()]);
            $this->db->commit();
            $this->id = $gameId;
            return $gameId;
        } catch (\PDOException $e) {
            $this->db->rollBack();
            $this->errors->add('Unable to create a game. If this happens ocasionally, please report an issue to administrators.');
            return 0;
        }
    }

    /**
     * Checks if user belongs to some game
     *
     * @param integer $userId
     * @return integer game id
     * Returns 0 if there is no game associated
     */
    public static function userBelongsToGame(int $userId): int
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT game_id FROM game_player WHERE user_id = :userId LIMIT 1";
        $data = $db->query($query, [':userId' => $userId])->fetchColumn();
        if (!$data) {
            return 0; // User is not at the table / in game
        }

        return (int) $data;
    }

    /**
     * Checks if user is the master of some game
     *
     * @param integer $userId
     * @return boolean
     */
    public static function isUserGameMaster(int $userId): bool
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT id FROM game WHERE user_id = :userId LIMIT 1";
        $data = $db->query($query, [':userId' => $userId])->fetchColumn();
        if (!$data)
            return false; // User is not the master of game / table

        return true;
    }

    /**
     * Removes player from the game
     * If it is master of the game, eliminates all game
     *
     * @return boolean
     */
    public function leave(): bool
    {
        if ($this->id < 1) {
            $this->errors->add('You do not belong to any of the tables.');
            return false;
        }

        try {
            $this->db->beginTransaction();

            // Do not remove table if only waiting for players and not the master that left
            if ($this->getStatus() === 'waiting_for_players' && !self::isUserGameMaster($this->user->getId())) {
                $query = "DELETE FROM game_player WHERE user_id = :userId";
                $this->db->query($query, [':userId' => $this->user->getId()]);
                $query = "INSERT INTO game_log (game_id, user_id, action)";
                $query .= " VALUES(:gameId, :userId, 'left_game')";
                $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId()]);
                $this->db->commit();
                return true;
            }

            $this->deck->remove();
            $query = "DELETE FROM game_log WHERE game_id = :gameId";
            $this->db->query($query, [':gameId' => $this->id]);
            $query = "DELETE FROM game_player WHERE game_id = :gameId";
            $this->db->query($query, [':gameId' => $this->id]);
            $query = "DELETE FROM game WHERE id = :gameId LIMIT 1";
            $this->db->query($query, [':gameId' => $this->id]);
            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollBack();
            $this->errors->add('For some reason system cannot remove you from the game. It would be wise to report this to the game administrators.');
            return false;
        }
    }

    /**
     * Gets all the active (waiting for players) games with corresponding usernames in the games
     * If there are more than 100 active games that waiting for players,
     * Display only limit of 200
     * Games are sorted from oldest to newest
     *
     * @return array
     */
    public function getGamesList(): array
    {
        if ($this->id > 0) {
            $this->errors->add('You are already at the table, so why you whish to see other ones?');
            return [];
        }

        $query = "SELECT DISTINCT gp.game_id AS game_id, GROUP_CONCAT(u.username) AS players";
        $query .= " FROM game_player AS gp";
        $query .= " INNER JOIN user AS u";
        $query .= " ON gp.user_id = u.id";
        $query .= " INNER JOIN game AS g";
        $query .= " ON gp.game_id = g.id";
        $query .= " WHERE g.status = 'waiting_for_players'";
        $query .= " AND (SELECT user_id FROM game_player WHERE user_id = g.user_id AND TIMESTAMPDIFF(SECOND, last_request_datetime, NOW()) <= 10)";
        $query .= " GROUP BY gp.game_id";
        $query .= " ORDER BY game_id ASC LIMIT 200";
        $games = $this->db->query($query)->fetchAll();

        if (!$games || empty($games)) {
            $this->errors->add('We are sorry, but currently there are no tables that are waiting for the players to join.');
            return [];
        }

        return $games;
    }

    /**
     * Checkcs if game exists by given ID
     *
     * @param integer $gameId
     * @param boolean $onlyNotInProgress
     * @return int
     * If there are no games, it returns 0
     * Otherwise it returns amount of the players that already joined to the game
     */
    public static function gameExists(int $gameId, bool $onlyNotInProgress = true): int
    {
        $db = \Core\MyPDO::instance();
        $query = "SELECT COUNT(gp.game_id) AS players_in_game FROM game AS g";
        $query .= " INNER JOIN game_player  AS gp";
        $query .= " ON g.id = gp.game_id";
        $query .= " WHERE g.id = :gameId";
        if ($onlyNotInProgress) {
            $query .= " AND status = 'waiting_for_players'";
        }
        $query .= " LIMIT 1";
        $data = $db->query($query, [':gameId' => $gameId])->fetchColumn();
        if (!$data)
            return 0; // Such game does not exist

        return $data;
    }

    /**
     * Retreaves data from database about the current Game
     *
     * @return void
     */
    private function getData()
    {
        try {
            $query = "SELECT u.username, g.user_id, g.status, g.current_turn_index, g.current_attacker_id, g.current_defender_id, g.is_defender_taking_home FROM game AS g";
            $query .= " INNER JOIN user AS u";
            $query .= " ON g.user_id = u.id";
            $query .= " WHERE g.id = :gameId LIMIT 1";
            $query .= " FOR UPDATE";
            $gameData = $this->db->query($query, [':gameId' => $this->id])->fetch();

            if (!$gameData) {
                throw new \Exception('Unable to get the data from the game. It&apos;s ID: ' . $this->id);
            }

            $this->status = $gameData['status'];
            $this->masterId = $gameData['user_id'];
            $this->masterUsername = $gameData['username'];
            $this->currentTurn = $gameData['current_turn_index'];
            $this->currentAttackerId = $gameData['current_attacker_id'];
            $this->currentDefenderId = $gameData['current_defender_id'];
            $this->defenderTakingHome = (bool) $gameData['is_defender_taking_home'];
        } catch (\Exception $e) {
            $this->errors->add($e->getMessage());
        }
    }

    /**
     * Gets ID of the master in the current game
     * If such game not yet exists, it returns null, because master will be the current player obviously
     * @return mixed
     */
    public function getMasterId()
    {
        $this->getData();
        return $this->masterId;
    }

    /**
     * Joins the player to the given game by game ID
     * Returns false if for some reason it's cannot be done
     *
     * @param integer $gameId
     * @return boolean
     */
    public static function join(int $gameId): bool
    {
        $errors = \Core\FormErrorCollector::instance();
        $db = \Core\MyPDO::instance();
        $user = \Core\Controller::globalModel('\App\Models\User');

        try {
            $db->beginTransaction();

            if (self::userBelongsToGame($user->getId())) {
                throw new \RuntimeException('You are already at the table. You cannot join or rejoin the table without leaving the table at first.');
                return false;
            }

            $playersInGame = self::gameExists($gameId);
            if ($playersInGame < 1) {
                throw new \RuntimeException('Such game does not exist or already in progress.');
                return false;
            }

            if ($playersInGame > self::PLAYERS_PER_GAME) {
                throw new \RuntimeException('Too many players would be at this table if you would join it. Please pick another table, or create your own table.');
                return false;
            }

            $query = "INSERT INTO game_player (game_id, user_id)";
            $query .= " VALUES(:gameId, :userId)";
            $data = $db->query($query, [':gameId' => $gameId, ':userId' => $user->getId()]);
            $query = "INSERT INTO game_log (game_id, user_id, action)";
            $query .= " VALUES(:gameId, :userId, 'joined_game')";
            $db->query($query, [':gameId' => $gameId, ':userId' => $user->getId()]);
            $db->commit();
            return true;
        } catch (\PDOException $e) {
            $db->rollBack();
            $errors->add('For some reason system do not allow you to join this table. If it strangely happens to you, please report this problem to administrators.');
            return false;
        } catch (\RuntimeException $e) {
            $db->rollBack();
            $errors->add($e->getMessage());
            return false;
        }
    }

    /**
     * Sets play order by random (decides who moves after hoom)
     * Saves that order in the database
     *
     * @return void
     */
    protected function setPlayOrder()
    {
        if (empty($this->players)) {
            return;
        }

        $playersTotal = count($this->players);
        if ($playersTotal < 2) {
            throw new \Exception('Unusual game order error. Game cannot be played with one player. Please report to the administrators.');
        }

        if ($playersTotal > self::PLAYERS_PER_GAME) {
            throw new \Exception('Game can be played with no more than ' . self::PLAYERS_PER_GAME . ' players at one table.');
        }

        $playersIds = [];
        foreach ($this->players as $player) {
            $playersIds[] = $player->getId();
        }
        shuffle($playersIds);

        $query = "UPDATE game_player SET turn_index = :turnIndex";
        $query .= " WHERE user_id = :userId LIMIT 1";
        $sth = $this->db->prepare($query);
        $i = 0;
        foreach ($playersIds as $userId) {
            $sth->execute([':userId' => $userId, ':turnIndex' => $i]);
            $this->players[$userId]->setTurnIndex($i);
            ++$i;
        }

        $query = "UPDATE game SET current_attacker_id = :currentAttackerId, current_defender_id = :currentDefenderId";
        $query .= " WHERE id = :gameId LIMIT 1";
        $this->db->query($query, [':gameId' => $this->id, ':currentAttackerId' => $playersIds[0], ':currentDefenderId' => $playersIds[1]]);
        $this->currentAttackerId = $playersIds[0];
        $this->currentDefenderId = $playersIds[1];
        $this->playOrder = $playersIds;
    }

    /**
     * Deals cards to all players in the game by automatically deciding how many cards player should get to keep 6 at the beginning of the round
     *
     * @return boolean
     * Returns true if cards were delt to players,
     * Or false on failure (someone won the game)
     */
    protected function dealCards(): bool
    {
        $hands = Player::getHands($this->id);

        // Draw cards in particular order that first draws the attacking player, then others (excluding defender) that sit clockwise and then lastly draws defender himself
        // Sort players by their indexes
        $players = [];
        $playerIndex = 0;
        foreach ($this->players as $player) {
            $players[$playerIndex] = $player;
            $players[$playerIndex]->gotCards = false;
            ++$playerIndex;
        }
        $playersTotal = count($players);
        $attackerIndex = $this->players[$this->currentAttackerId]->getTurnIndex();
        $defenderIndex = $this->players[$this->currentDefenderId]->getTurnIndex();
        $nextPlayerIndex = $attackerIndex;
        $playersGotCards = 0; // How many players already got cards
        do {
            if (
                $playersGotCards === 0 ||
                ($nextPlayerIndex !== $attackerIndex && $nextPlayerIndex !== $defenderIndex && $players[$nextPlayerIndex]->gotCards === false) ||
                $nextPlayerIndex === $defenderIndex && $playersTotal - 1 === $playersGotCards
            ) {
                $userId = $players[$nextPlayerIndex]->getId();
                $hand = $hands[$nextPlayerIndex]['cards'];
                $cards = explode(',', $hand);
                $cardsInHand = count($cards);
                if ($cardsInHand === 1 && $cards[0] === '') { // Actually has no cards
                    $cardsInHand = 0;
                }

                if ($cardsInHand < 6) { // everyone should have at least 6 cards
                    $cardsDrawn = $this->deck->drawCards(6 - $cardsInHand);
                    if (empty($cardsDrawn)) { // There are no more cards in Deck so other players can not get anything from it
                        break;
                    }

                    $players[$nextPlayerIndex]->addCards($cardsDrawn);
                }
                ++$playersGotCards; // Perhaps someone do not need cards, that's why it outside the condition of having < 6 cards
                $players[$nextPlayerIndex]->gotCards = true;
            }

            // Determine who sits next to the current player
            if ($nextPlayerIndex === $playersTotal - 1) { // Last player clockwise
                $nextPlayerIndex = 0;
            } else {
                $nextPlayerIndex = $nextPlayerIndex + 1;
            }
        } while ($playersTotal > $playersGotCards); // Continue while there is at least one player who didn't get the cards and cards still exist in deck

        $cardsInDeck = $this->deck->save();

        if ($this->checkWinners()) {
            return false;
        }

        return true;
    }

    /**
     * Initiates game progress:
     * Deals 6 cards to each player,
     * Determines play order (who goes after whoom),
     * Changes status to 'in_progress',
     * Removes all the old data from the log,
     * Informs all players by using game log
     *
     * @return boolean
     */
    public function start(): bool
    {
        try {
            $this->db->beginTransaction();

            $this->getData();

            if ($this->user->getId() !== $this->masterId) {
                throw new \Exception('You are not the master of the game, so you cannot start it.');
            }

            $playersTotal = count($this->players);
            if ($playersTotal < 2) {
                throw new \Exception('You cannot start the game yet, because game can be played with no less than 2 players.');
            }

            if ($playersTotal > self::PLAYERS_PER_GAME) {
                throw new \Exception('Game can be played with no more than ' . self::PLAYERS_PER_GAME . ' players.');
            }

            if ($this->getStatus() !== 'waiting_for_players') {
                throw new \Exception('Current game state is not waiting for players, so you cannot start the game. This is unusual error, please report to the administrators.');
            }

            if (!$this->deck instanceof Deck) {
                throw new \Exception('For some reason it is impossible to start the game, because deck is not successfully prepared for the game. This is unusual error, please report to administrators.');
            }

            $this->setPlayOrder();
            $this->dealCards();

            $query = "UPDATE game SET status = 'in_progress'";
            $query .= " WHERE id = :gameId LIMIT 1";
            $this->db->query($query, [':gameId' => $this->id]);

            $query = "DELETE FROM game_log";
            $query .= " WHERE game_id = :gameId";
            $this->db->query($query, [':gameId' => $this->id]);

            $query = "INSERT INTO game_log (game_id, user_id, action)";
            $query .= " VALUES(:gameId, :userId, 'started_game')";
            $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId()]);

            $this->status = 'in_progress';
            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollBack();
            $this->errors->add('For some reason there is impossible to mark game as started. Please report this error to administrators.');
            return false;
        } catch (\Exception $e) {
            $this->errors->add($e->getMessage());
            return false;
        }
    }

    /**
     * Return Player object of the player who should make a current turn
     *
     * @return mixed
     * Returns Player object on success
     * or null on failure
     */
    public function getPlayerOfCurrentTurn()
    {
        if (empty($this->players)) {
            return null;
        }

        foreach ($this->players as $player) {
            if ($player->getTurnIndex() === $this->currentTurn) {
                return $player;
            }
        }

        return null;
    }

    /**
     * Sets cards that are already in play (on the table) as Card objects
     * 
     * @return array (cards in play)
     */
    private function getCardsInPlay(): array
    {
        if (!empty($this->cardsInPlay)) {
            return $this->cardsInPlay;
        }

        $query = "SELECT data AS code FROM game_log";
        $query .= " WHERE game_id = :gameId AND action IN('played_card_atk', 'played_card_def', 'played_card_skip', 'played_card_home')";
        $query .= " ORDER BY id ASC";
        $cards = $this->db->query($query, [':gameId' => $this->id])->fetchAll();
        if (!$cards) {
            $this->cardsInPlay = [];
            return $this->cardsInPlay;
        }

        $cardsInPlay = [];
        foreach ($cards as $card) {
            $cardsInPlay[$card['code']] = new Card($card['code']);
            // Set trump if needed
            if ($cardsInPlay[$card['code']]->getSuit() === $this->trumpCard->getSuit()) {
                $cardsInPlay[$card['code']]->setTrump(true);
            }
        }

        $this->cardsInPlay = $cardsInPlay;
        return $this->cardsInPlay;
    }

    /**
     * Gets cards on the table (for sending to the client)
     * These cards are returned with their corresponding actions (played_card_atk, played_card_def, played_card_skip)
     * 
     * @return array
     */
    public function getCardsOnTheTable(): array
    {
        $query = "SELECT action, data AS code FROM game_log";
        $query .= " WHERE game_id = :gameId AND action IN('played_card_atk', 'played_card_def', 'played_card_skip', 'played_card_home')";
        $query .= " ORDER BY id ASC";
        $cards = $this->db->query($query, [':gameId' => $this->id])->fetchAll();
        if (!$cards) {
            return [];
        }

        $cardsInPlay = [];
        foreach ($cards as $card) {
            $cardsInPlay[] = [$card['action'], $card['code']];
        }

        return $cardsInPlay;
    }

    /**
     * Checks if attacker can use the given card
     * He can play the card if there are the same card ranks on the table,
     * Or the card is the first to be played, so it doesn't matter which one it is
     *
     * @param Card $attackerCard
     * @return boolean
     */
    protected function canAttackerPlayCard(Card $attackerCard): bool
    {
        if (empty($this->cardsInPlay)) { // There are no cards in play
            return true;
        }

        foreach ($this->cardsInPlay as $card) {
            if ($attackerCard->getRank() === $card->getRank()) { // Follows rank
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if defender can use the given card
     * He can play the card if there is same card suit on the table that attacker played and defender suit is more powerfull,
     * Also If defender card is trump and attacker's card is not or attacker's trump is weeker,
     * Or if Attacker's card rank is the same as defenders card rank (then skipping happens)
     *
     * @param Card $defenderCard
     * @return mixed
     * Boolean true if player can regulary defend with the card
     * Boolean false if player can not defend with the card
     * Integer 1 if player completely defends against the passings / skippings
     * Integer 2 if player defends against passings, but should continue on that
     * integer -1 if player can skip the turn
     * @throws Exception
     */
    protected function canDefenderPlayCard(Card $defenderCard)
    {
        $playersTotal = count($this->players);
        $cardsTotal = count($this->cardsInPlay);
        $defendAgainstIndex = -1;
        $sameRanks = 0;

        // Sort cards by their indexes and check for same ranks
        $cardsInPlay = [];
        $i = 0;
        $stillSameRank = true; // If ranks are not going in row, we make this false to not continue counting them
        foreach ($this->cardsInPlay as $card) {
            if ($i > 0 && $stillSameRank && $card->getRank() === $cardsInPlay[0]->getRank()) {
                ++$sameRanks;
            } else if ($stillSameRank && $i > 0) {
                $stillSameRank = false;
            }
            $cardsInPlay[] = $card;
            ++$i;
        }
        ++$sameRanks; // First card was not included

        // Check if during this round someone skipped their turn by passing with the card
        if ($sameRanks > 1) {
            $defendAgainstIndex = $cardsTotal - $sameRanks;
            if ($defendAgainstIndex >= $sameRanks) { // Not the defence against passings anymore
                $defendAgainstIndex = -1;
            }
        }

        // Decide against which card defender should defend
        if ($defendAgainstIndex === -1) { // Regular defense
            $attackerCard = $cardsInPlay[$cardsTotal - 1]; // Last playd card
        } else { // Defense against passings / skippings
            $attackerCard = $cardsInPlay[$defendAgainstIndex];
        }

        // Check if defender skips his turn by passing with the card of same rank and there are no other ranks in play (to make sure he can skip the turn)
        if ($sameRanks === $cardsTotal && $defenderCard->getRank() === $attackerCard->getRank()) {
            return -1; // Skips turn
        }

        // Check if defender's card follows suit or trump
        if ($defenderCard->getSuit() === $attackerCard->getSuit() || $defenderCard->isTrump()) {
            // Check if defender's card is stronger or not
            if ($defenderCard->getScore() <= $attackerCard->getScore()) {
                throw new \RuntimeException('You cannot defend with the weeker card.');
                return false;
            }

            if ($defendAgainstIndex !== -1) { // Defends against skipps / passings
                if ($sameRanks - 1 === $defendAgainstIndex) { // Last passing card to beat
                    return 1;
                }

                return 2; // Not the last passing card
            }

            return true; // Defender's card is more powerful
        }

        return false; // Must follow the suit
    }

    /**
     * Adds player card to the Game log (virtually puts it on the table) if card can be played
     *
     * @param string $cardCode
     * @return mixed
     * If object is return as Card, it means that card was played successfully
     * Otherwise boolean false is returned to indicate failure
     * @throws Exception
     */
    public function playCard(string $cardCode)
    {
        try {
            $this->db->beginTransaction();

            $this->getData();

            $status = $this->getStatus();
            if (empty($this->players) || ($status !== 'in_progress' && $status !== 'finished')) {
                throw new \Exception('You are not playing at the moment.');
            }

            // Check if someone had already won
            if ($status === 'finished') {
                throw new \Exception('Winner is already determined, so game is over.');
            }

            // Check if it is the current player's turn
            if ($this->players[$this->user->getId()]->getTurnIndex() !== $this->currentTurn) {
                throw new \Exception('It is not your turn.');
            }

            // Check if player can play the card (defender still has some cards in hand)
            if ($this->user->getId() === $this->currentAttackerId && count($this->players[$this->currentDefenderId]->getCardsInHand()) < 1) {
                throw new \Exception('Defender has no more cards to counter your attack, you should pass.');
            }

            // Check if player actually has the card he pretends to own
            if (!$this->players[$this->user->getId()]->hasCard($cardCode)) {
                throw new \Exception('You have no such card!');
            }

            $this->getCardsInPlay(); // Retrieve info about the cars on the table
            $card = new Card($cardCode); // Playing card
            if ($card->getSuit() === $this->trumpCard->getSuit()) { // Set as trump
                $card->setTrump(true);
            }
            $ret = false; // Can't play card yet (we don't know if he can)

            // Check if player is attacking or defending
            if ($this->user->getId() === $this->currentAttackerId) { // Player attacking
                // Check if attacker can use this card acording to rule that says to follow ranks
                if (!$this->canAttackerPlayCard($card)) {
                    throw new \RuntimeException('You cannot play this card. You must follow the rank...');
                }

                // Check if player throws cards to a defender or it is regular attack
                if ($this->defenderTakingHome) {
                    $action = 'played_card_home';
                    $nextAttackerId = $this->currentAttackerId;
                    $nextDefenderId = $this->currentDefenderId;
                    $nextTurn = $this->players[$nextAttackerId]->getTurnIndex(); // Continue to throw cards
                } else {
                    $action = 'played_card_atk';
                    $nextAttackerId = $this->currentAttackerId;
                    $nextDefenderId = $this->currentDefenderId;
                    $nextTurn = $this->players[$nextDefenderId]->getTurnIndex(); // Next turn is for a defender
                }
            } else { // Player defending with the card
                // Check if defender can use this card to skip his turn or defend acording to rule that says to follow greater suit or trump
                $ret = $this->canDefenderPlayCard($card);
                if ($ret === false) {
                    throw new \RuntimeException('You cannot play this card. You must follow the suit...');
                } else if ($ret === -1) { // Skips turn 
                    // Sort players by their indexes
                    $players = [];
                    foreach ($this->players as $player) {
                        $players[] = $player;
                    }
                    $playersTotal = count($players);

                    // Determine player who should defend when current defender skips his turn and who should attack later (that's the same player who's skippping now)
                    if ($this->players[$this->currentDefenderId]->getTurnIndex() === $playersTotal - 1) { // Last player clockwise
                        $nextDefenderIndex = 0;
                    } else {
                        $nextDefenderIndex = $this->players[$this->currentDefenderId]->getTurnIndex() + 1;
                    }
                    $nextDefenderId = $players[$nextDefenderIndex]->getId();
                    $nextAttackerId = $this->currentDefenderId; // Current defender will attack after the next defender will defeats all the skipppings
                    $nextTurn = $nextDefenderIndex; // Player possibly skips so pretend that then should go next defender (if later code will not change this state)

                    // Check if new defender has needed amount of cards to beat passed cards (if this card would be plased), or he otherwise current defender can defend (by using trump)
                    $cardsInPlayTotal = count($this->cardsInPlay);
                    if (count($this->players[$nextDefenderId]->getCardsInHand()) < $cardsInPlayTotal + 1) {
                        if ($card->isTrump()) { // Allow to place trump to defend
                            $action = 'played_card_def';
                            $nextAttackerId = $this->currentAttackerId;
                            $nextDefenderId = $this->currentDefenderId;
                            if ($cardsInPlayTotal === 1) { // Attacker should go, because there are no cards to beat
                                $nextTurn = $this->players[$nextAttackerId]->getTurnIndex();
                            } else { // There are cards to beat, so defender should go
                                $nextTurn = $this->players[$nextDefenderId]->getTurnIndex();
                            }
                        } else {
                            throw new \Exception('You cannot use skipping, because defender has less cards in hand than needs to defend.');
                        }
                    } else { // Let him skip
                        $action = 'played_card_skip';
                    }
                } else if ($ret === 2) { // Let defender continue, because there are cards to be defended against
                    $action = 'played_card_def';
                    $nextAttackerId = $this->currentAttackerId;
                    $nextDefenderId = $this->currentDefenderId;
                    $nextTurn = $this->players[$nextDefenderId]->getTurnIndex(); // Since player defended successfully, let atacker to continue his attack
                } else { // Regular defense
                    $action = 'played_card_def';
                    $nextAttackerId = $this->currentAttackerId;
                    $nextDefenderId = $this->currentDefenderId;
                    $nextTurn = $this->players[$nextAttackerId]->getTurnIndex(); // Since player defended successfully, let atacker to continue his attack
                }
            }

            // Update game info
            $query = "UPDATE game SET current_turn_index = :nextTurn, current_attacker_id = :nextAttackerId, current_defender_id = :nextDefenderId";
            $query .= " WHERE id = :gameId LIMIT 1";
            $this->db->query($query, [':gameId' => $this->id, ':nextTurn' => $nextTurn, ':nextAttackerId' => $nextAttackerId, ':nextDefenderId' => $nextDefenderId]);

            // Remove card from player hand
            $this->players[$this->user->getId()]->removeCardFromHand($card->getCode());

            // Add card to game log (on the table)
            $query = "INSERT INTO game_log (game_id, user_id, action, data)";
            $query .= " VALUES (:gameId, :userId, :action, :card)";
            $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId(), ':action' => $action, ':card' => $card->getCode()]);

            // Mark the players that they didn't passed any turns during round to make sure full circle around didn't play any cards
            if (!$this->defenderTakingHome) {
                $query = "UPDATE game_player SET passed = FALSE";
                $query .= " WHERE game_id = :gameId AND passed = TRUE";
                $this->db->query($query, [':gameId' => $this->id]);
            }

            $this->db->commit();
            return $card;
        } catch (\PDOException $e) {
            $this->db->rollBack();
            $this->errors->add('For some reason you cannot play the card. Please report this to the administrators.');
            return false;
        } catch (\Exception $e) {
            $this->db->rollBack();
            $this->errors->add($e->getMessage());
            return false;
        } catch (\RuntimeException $e) {
            $this->db->rollBack();
            $this->errors->add($e->getMessage());
            return false;
        }
    }

    /**
     * Passes the turn for the player if he can do so
     * Or if all the players had passed (except defender of course), it does bat
     *
     * @return boolean
     * @throws Exception
     */
    public function passTurn(): bool
    {
        try {
            $this->db->beginTransaction();

            $this->getData();

            $status = $this->getStatus();
            if (empty($this->players) || ($status !== 'in_progress' && $status !== 'finished')) {
                throw new \Exception('You are not playing at the moment.');
            }

            // Check if someone had won
            if ($status === 'finished') {
                throw new \Exception('Winner is already determined, so game is over.');
            }

            // Check if it is the current player's turn
            if ($this->players[$this->user->getId()]->getTurnIndex() !== $this->currentTurn) {
                throw new \Exception('It is not your turn.');
            }

            // Player should not be defender to be able to pass
            if ($this->user->getId() === $this->currentDefenderId) {
                throw new \Exception('You cannot  pass, because you are defending.');
            }

            // Check if there are no cards on the table and player tries to pass his turn
            if (empty($this->getCardsInPlay())) {
                throw new \Exception('You cannot pass your first attack.');
            }

            // Check if all players (except defender) had passed, so should be bat, or defender should take cards home if he's taking
            $players = [];
            $playersTotal = count($this->players);
            $passed = 1; // Current player is passing now, so incalculate him
            foreach ($this->players as $player) {
                if ($player->getPassed()) {
                    ++$passed;
                }
                $players[] = $player;
            }

            if ($passed === $playersTotal - 1) { // Exclude defender
                if ($this->defenderTakingHome) {
                    $this->takeHome();
                } else {
                    $this->doBat($players);
                }
                $this->db->commit();
                return true;
            }

            // Find the player who should get the move after pass
            if ($this->players[$this->user->getId()]->getTurnIndex() === $playersTotal - 1) { // Last player clockwise
                $nextPlayerIndex = 0;
            } else {
                $nextPlayerIndex = $this->players[$this->user->getId()]->getTurnIndex() + 1;
            }

            // Check if picked player is not defending
            if ($players[$nextPlayerIndex]->getId() === $this->currentDefenderId) { // Pick next one, because this player is defending
                ++$nextPlayerIndex;
            }

            // Check if picked player exists and if not, pick index as 0 (because last checked player is the last in clockwise order)
            $nextPlayerIndex = $nextPlayerIndex >= $playersTotal ? 0 : $nextPlayerIndex;

            // Mark the player that he passed the turn
            $query = "UPDATE game_player SET passed = TRUE";
            $query .= " WHERE user_id = :userId LIMIT 1";
            $this->db->query($query, [':userId' => $this->user->getId()]);

            // Let other player to make his attack
            $query = "UPDATE game SET current_turn_index = :nextPlayerIndex, current_attacker_id = :currentAttackerId";
            $query .= " WHERE id = :gameId LIMIT 1";
            $this->db->query($query, [':nextPlayerIndex' => $nextPlayerIndex, ':currentAttackerId' => $players[$nextPlayerIndex]->getId(), ':gameId' => $this->id]);

            // Log what's happened
            $query = "INSERT INTO game_log (game_id, user_id, action)";
            $query .= " VALUES(:gameId, :userId, 'passed_turn')";
            $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId()]);

            $this->db->commit();
            return true;
        } catch (\PDOException $e) {
            $this->db->rollBack();
            $this->errors->add('For some reason you cannot pass the turn. Please report this issue to administrators.');
            return false;
        } catch (\Exception $e) {
            $this->errors->add($e->getMessage());
            return false;
        }
    }

    /**
     * Player lets to throw additional cards at him, because he takes home
     *
     * @return void
     * @throws Exception
     */
    public function throwHome()
    {
        try {
            $this->db->beginTransaction();

            $this->getData();

            if (empty($this->players) || $this->getStatus() !== 'in_progress') {
                throw new \Exception('You are not playing at the moment.');
            }

            // Check if it is the current player's turn
            if ($this->players[$this->user->getId()]->getTurnIndex() !== $this->currentTurn) {
                throw new \Exception('It is not your turn.');
            }

            // Check if player is defending
            if ($this->user->getId() !== $this->currentDefenderId) {
                throw new \Exception('You are not defending, so you cannot take cards home.');
            }

            $this->getCardsInPlay(); // Retrieve info about the cars on the table

            // Check if there are cards to take
            if (empty($this->cardsInPlay)) {
                throw new \Exception('There are no cards on the table to take.');
            }

            $nextAttackerId = $this->currentAttackerId;
            $nextDefenderId = $this->currentDefenderId;
            $nextTurn = $this->players[$nextAttackerId]->getTurnIndex();

            // Let attacker to make a move, mark that current defender is taking cards home
            $query = "UPDATE game SET current_turn_index = :nextTurnIndex, current_attacker_id = :nextAttackerId, current_defender_id = :nextDefenderId, is_defender_taking_home = TRUE";
            $query .= " WHERE id = :gameId LIMIT 1";
            $this->db->query($query, [':gameId' => $this->id, ':nextTurnIndex' => $nextTurn, ':nextAttackerId' => $nextAttackerId, ':nextDefenderId' => $nextDefenderId]);

            // Mark the players that they didn't pass any turns during the new round
            $query = "UPDATE game_player SET passed = FALSE";
            $query .= " WHERE game_id = :gameId AND passed = TRUE";
            $this->db->query($query, [':gameId' => $this->id]);

            // Log what's happened
            $query = "INSERT INTO game_log (game_id, user_id, action)";
            $query .= " VALUES(:gameId, :userId, 'taking_home')";
            $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId()]);

            $this->db->commit();
        } catch (\PDOException $e) {
            $this->db->rollBack();
            $this->errors->add('For some reason you cannot take cards home. Please report this to the administrators.');
        } catch (\Exception $e) {
            $this->db->rollBack();
            $this->errors->add($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->db->rollBack();
            $this->errors->add($e->getMessage());
        }
    }

    /**
     * Takes cards home for the player if he can and starts new round
     * Also checks if there are no winners yet and if not:
     * Determines new attacker and defender for the next round
     * Deals cards to the players to hold 6
     *
     * @return boolean
     */
    protected function takeHome(): bool
    {
        $this->getCardsInPlay(); // Retrieve info about the cars on the table

        // Give cards to the player and remove them from table
        $this->players[$this->currentDefenderId]->addCards($this->cardsInPlay);
        $this->cardsInPlay = [];
        $query = "DELETE FROM game_log";
        $query .= " WHERE game_id = :gameId AND action != 'chat'";
        $this->db->query($query, [':gameId' => $this->id]);

        // Deal cards to players for the next round
        $this->dealCards();

        // Sort players by their indexes
        $players = [];
        foreach ($this->players as $player) {
            $players[] = $player;
        }
        $playersTotal = count($players);

        // Determine players who should attack and who should defend when current defender takes all cards home (there are players on current defender's left)
        if ($this->players[$this->currentDefenderId]->getTurnIndex() === $playersTotal - 1) { // Last player clockwise
            $nextAttackerIndex = 0;
        } else {
            $nextAttackerIndex = $this->players[$this->currentDefenderId]->getTurnIndex() + 1;
        }
        $nextAttackerId = $players[$nextAttackerIndex]->getId();
        if ($this->players[$nextAttackerId]->getTurnIndex() === $playersTotal - 1) { // Last player clockwise
            $nextDefenderIndex = 0;
        } else {
            $nextDefenderIndex = $this->players[$nextAttackerId]->getTurnIndex() + 1;
        }
        $nextDefenderId = $players[$nextDefenderIndex]->getId();

        // Let attacker to make a move, save who will defend and mark that defender is no longer taking home
        $query = "UPDATE game SET current_turn_index = :nextTurnIndex, current_attacker_id = :nextAttackerId, current_defender_id = :nextDefenderId, is_defender_taking_home = FALSE";
        $query .= " WHERE id = :gameId LIMIT 1";
        $this->db->query($query, [':gameId' => $this->id, ':nextTurnIndex' => $nextAttackerIndex, ':nextAttackerId' => $nextAttackerId, ':nextDefenderId' => $nextDefenderId]);

        // Mark the players that they didn't pass any turns during the new round
        $query = "UPDATE game_player SET passed = FALSE";
        $query .= " WHERE game_id = :gameId AND passed = TRUE";
        $this->db->query($query, [':gameId' => $this->id]);

        // Log what's happened
        $query = "INSERT INTO game_log (game_id, user_id, action)";
        $query .= " VALUES(:gameId, :userId, 'took_home')";
        $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->players[$this->currentDefenderId]->getId()]);

        return true;
    }

    /**
     * Checks if there are no winners,
     * If there are winners, then it ends the current game.
     * Otherwise:
     * Does bat (ends the current round and sets everything in order to go on a next one):
     * Finds and sets the next attacker and defender
     * Removes previous round story
     * Automatically draws the cards from the deck for the players (to hold 6)
     *
     * @param array $players
     * @return boolean
     * @throws Exception
     */
    protected function doBat(array $players): bool
    {
        // Find the player who should be defender after bat (it is the player that sits clockwise to the previous defender)
        $nextAttackerId = $this->currentDefenderId;
        if ($this->players[$nextAttackerId]->getTurnIndex() === count($players) - 1) { // Last player clockwise
            $nextDefenderIndex = 0;
        } else {
            $nextDefenderIndex = $this->players[$nextAttackerId]->getTurnIndex() + 1;
        }
        $nextDefenderId = $players[$nextDefenderIndex]->getId();

        // Let players to draw the cards to hold 6
        if (!$this->dealCards()) { // Perhaps someone won the game 
            return true;
        }

        // Mark the players that they didn't passed any turns during the new round
        $query = "UPDATE game_player SET passed = FALSE";
        $query .= " WHERE game_id = :gameId AND passed = TRUE";
        $this->db->query($query, [':gameId' => $this->id]);

        // Let previous defender to make his attack and mark that next player that sits after the previous defender (clockwise) will be defender during this round
        $query = "UPDATE game SET current_turn_index = :nextAttackerIndex, current_attacker_id = :nextAttackerId, current_defender_id = :nextDefenderId";
        $query .= " WHERE id = :gameId LIMIT 1";
        $this->db->query($query, [':nextAttackerIndex' => $this->players[$nextAttackerId]->getTurnIndex(), ':nextAttackerId' => $nextAttackerId, ':nextDefenderId' => $nextDefenderId, ':gameId' => $this->id]);

        // Remove log actions (cards, passes and so on of the previous round, except chat history)
        $query = "DELETE FROM game_log";
        $query .= " WHERE game_id = :gameId AND action != 'chat'";
        $this->db->query($query, [':gameId' => $this->id]);

        // Log what's happened
        $query = "INSERT INTO game_log (game_id, user_id, action, data)";
        $query .= " VALUES(:gameId, :userId, 'bat', :data)";
        $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId(), ':data' => $this->players[$this->currentDefenderId]->getUsername()]);

        return true;
    }

    /**
     * Checks if someone had won already
     * If there is some player who won the game (or players got the draw),
     * It adds this information to the log and changes game status to finished
     * Removes the deck (when master starts game again, it will be renewed)
     *
     * @return boolean
     */
    protected function checkWinners(): bool
    {
        $lastWinnerId = false;
        $winnersUsernames = [];
        if ($this->deck->countCards() === 0) {
            foreach ($this->players as $player) {
                if (count($player->getCardsInHand()) === 0) {
                    $lastWinnerId = $player->getId();
                    $winnersUsernames[] = $player->getUsername();
                }
            }
            $winners = count($winnersUsernames);
            if ($winners > 0) {
                if ($winners > 1) { // Draw
                    $query = "INSERT INTO game_log (game_id, action, data)";
                    $query .= " VALUES(:gameId, 'draw', :winnersUsernames)";
                    $this->db->query($query, [':gameId' => $this->id, ':winnersUsernames' => implode(',', $winnersUsernames)]);
                } else { // We have a winner
                    $query = "INSERT INTO game_log (game_id, user_id, action)";
                    $query .= " VALUES(:gameId, :userId, 'won')";
                    $this->db->query($query, [':gameId' => $this->id, ':userId' => $lastWinnerId]);
                }

                $query = "UPDATE game SET status = 'finished'";
                $query .= " WHERE id = :gameId LIMIT 1";
                $this->db->query($query, [':gameId' => $this->id]);
                //$this->deck->remove();
                return true;
            }
        }

        return false;
    }

    /**
     * Sends given message to all players in the table
     *
     * @param string $message
     * @return bool message was sent or not
     * @throws Exception
     */
    public function sendChatMessage(string $message): bool
    {
        try {
            $message = trim($message);
            $messageLen = mb_strlen($message);
            if ($messageLen < 2) {
                throw new \Exception('Message is too short, no less than 2 characters are required.');
            }

            if ($messageLen > 100) {
                throw new \Exception('Message is too long, no more than 100 characters are allowed.');
            }

            $query = "INSERT INTO game_log (game_id, user_id, action, data)";
            $query .= " VALUES(:gameId, :userId, 'chat', :message)";
            $this->db->query($query, [':gameId' => $this->id, ':userId' => $this->user->getId(), ':message' => $message]);
            return true;
        } catch (\PDOException $e) {
            $this->errors->add('Unable to send message. Please report this issue to administrators.');
            return false;
        } catch (\Exception $e) {
            $this->errors->add($e->getMessage());
            return false;
        }
    }

    /**
     * Gets newest game log ID that will be used as the request key in AJAX call
     *
     * @param integer $gameId
     * If it is 0, current game ID will be used, otherwise - passed ID will be used
     * It is useful to let user that is not at the game yet to skip some game log story
     * @return string
     * Returns empty string if there is no such data
     */
    public function getNewestRequestKey(int $gameId = 0): string
    {
        if ($gameId == 0) {
            $gameId = $this->id;
        }

        $query = "SELECT MAX(id) FROM game_log";
        $query .= " WHERE game_id = :gameId LIMIT 1";
        $requestKey = $this->db->query($query, [':gameId' => $gameId])->fetchColumn();
        if (!$requestKey) {
            return '';
        }

        return (string) $requestKey;
    }

    /**
     * Gets game status
     * For example game is 'in_progress', 'waiting_for_players'...
     *
     * @return string
     */
    public function getStatus(): string
    {
        if ($this->id < 1) {
            return 'not_exist';
        }

        $query = "SELECT status FROM game WHERE id = :gameId LIMIT 1";
        $status = $this->db->query($query, [':gameId' => $this->id])->fetchColumn();
        if (!$status) {
            return 'not_exist';
        }

        return (string) $status;
    }

    /**
     * Gets all log records about the current game, determined by the log ID, that client saw lastly
     * It retrieves only greater results than that ID
     * If ID is empty or didn't pass as a parameter,
     * Then method returns all the log records about the current game
     * Results are sorted from oldest to newest
     *
     * @param string $logId
     * @return array
     */
    public function getGameLog(string $logId = ''): array
    {
        if ($this->id < 1) {
            $this->errors->add('There is no table to get log history from.');
            return [];
        }

        if ($logId == '') { // No particular ID, so requests for all the log records of the current game
            $logId = '0';
        }

        $query = "SELECT gl.*, u.username FROM game_log AS gl";
        $query .= " LEFT JOIN user AS u";
        $query .= " ON gl.user_id = u.id";
        $query .= " WHERE gl.game_id = :gameId AND gl.id > :logId";
        $query .= " ORDER BY id ASC";
        $log = $this->db->query($query, [':gameId' => $this->id, ':logId' => $logId])->fetchAll();
        if (!$log || empty($log)) {
            $this->errors->add('We are sorry, but currently there is no information about the actions on table that players did.');
            return [];
        }

        return $log;
    }

    /**
     * Gets pulling to the server results acording to the status and given last request key (lrkey)
     *
     * @return \stdClass
     */
    public function getPullingResults(): \stdClass
    {
        $status = [];
        $status[0] = $this->getNewestRequestKey();
        $status[1] = $this->getStatus();
        $gameObj = new \stdClass();
        $gameObj->userId = $this->user->getId();
        $gameObj->username = $this->user->getUsername();
        $gameObj->lrkey = $status[0];
        $gameObj->status = $status[1];
        $lrkey = !empty($_POST['lrkey']) ? (string) $_POST['lrkey'] : '';

        // Update last request datetime for the current player
        $query = "UPDATE game_player SET last_request_datetime = NOW()";
        $query .= " WHERE user_id = :userId LIMIT 1";
        $this->db->query($query, [':userId' => $this->user->getId()]);

        // Get players whom suddenly disconnected if they did
        $query = "SELECT username FROM game_player AS gp";
        $query .= " INNER JOIN user AS u";
        $query .= " ON gp.user_id = u.id";
        $query .= " WHERE TIMESTAMPDIFF(SECOND, gp.last_request_datetime, NOW()) > 10 AND game_id = :gameId"; // Player is considered to lost his internet connection if he does not make request for > 10 s
        $brokenPlayers = $this->db->query($query, [':gameId' => $this->id])->fetchAll();

        if ($brokenPlayers) {
            $playersUsernames = [];
            foreach ($brokenPlayers as $player) {
                $playersUsernames[] = $player['username'];
            }
            $gameObj->brokenPlayers = $playersUsernames;
        }

        if ($lrkey !== '' && $lrkey === $status[0]) { // Key is the same in the server that client sent, no need for new info
            $gameObj->nothingChanged = true;
            return $gameObj;
        }

        switch ($status[1]) {
                // User does not belong to any of the games
            case '':

                break;

                // Server indicates that everybuddy in this game are waiting for new players
            case 'waiting_for_players':
                $this->getData();
                $gameObj->gamePlayers = Player::getGamePlayers($this->id);
                $gameObj->log = $this->getGameLog($lrkey);
                break;

                // Actual game is in progress
            case 'in_progress':
            case 'finished':
                $this->getData();
                $playerWhoShouldMove = $this->getPlayerOfCurrentTurn();
                if (!empty($playerWhoShouldMove)) {
                    $gameObj->currentTurnPlayerId = $playerWhoShouldMove->getId();
                    $gameObj->currentTurnPlayerUsername = $playerWhoShouldMove->getUsername();
                }
                $gameObj->trumpCard = $this->trumpCard->getCode();
                $gameObj->cardsInDeck = $this->deck->countCards();
                $cardsOnTheTable =  $this->getCardsOnTheTable();
                if (!empty($cardsOnTheTable)) {
                    $gameObj->cardsInPlay = $cardsOnTheTable;
                }

                $gameObj->gamePlayers = Player::getGamePlayers($this->id);
                $gameObj->log = $this->getGameLog($lrkey);
                $gameObj->playerCards = $this->players[$this->user->getId()]->getCardsInHand();
                break;

            default:
                break;
        }

        return $gameObj;
    }
}
