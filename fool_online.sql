-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 15, 2020 at 07:21 PM
-- Server version: 8.0.21-0ubuntu0.20.04.4
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fool_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `id` int UNSIGNED NOT NULL,
  `user_id` mediumint UNSIGNED NOT NULL COMMENT 'Table master ID',
  `status` enum('waiting_for_players','in_progress','finished','rejected') COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_turn_index` tinyint UNSIGNED DEFAULT '0' COMMENT 'Index of the player which is currently playing (count starts from 0)',
  `current_attacker_id` mediumint UNSIGNED DEFAULT NULL COMMENT 'Player who attacks',
  `current_defender_id` mediumint UNSIGNED DEFAULT NULL COMMENT 'To be able to know which player should go after the end of current round.',
  `is_defender_taking_home` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determines if defender is currently taking cards home so others can throw additional cards.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `game_deck`
--

CREATE TABLE `game_deck` (
  `game_id` int UNSIGNED NOT NULL,
  `cards` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Cards in CSV format, card code consists of 2 chars (rank and suit)',
  `trump_card` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Code of the card that will be used as trump.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `game_log`
--

CREATE TABLE `game_log` (
  `id` bigint UNSIGNED NOT NULL COMMENT 'Use this ID as the key for AJAX requests to indicate what client / user has as the last piece of the story',
  `game_id` int UNSIGNED NOT NULL,
  `user_id` mediumint UNSIGNED DEFAULT NULL,
  `action_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `action` enum('created_game','joined_game','left_game','started_game','played_card_atk','played_card_def','played_card_skip','taking_home','played_card_home','took_home','passed_turn','bat','won','draw','chat') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'All the actions that can happen during the game',
  `data` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Data of the given action, for instance an actual card that user played'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `game_player`
--

CREATE TABLE `game_player` (
  `game_id` int UNSIGNED NOT NULL,
  `user_id` mediumint UNSIGNED NOT NULL,
  `cards` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Cards in player''s hand',
  `turn_index` tinyint UNSIGNED DEFAULT NULL COMMENT 'Determines when it is player''s turn to play a card',
  `passed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'To check if player already passed in the current round (by going clockwise)',
  `last_request_datetime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Updates which each data pull to determine which user had lost connection'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Can be 22 up to 40 chars of length',
  `session_expiration_datetime` datetime NOT NULL COMMENT 'Add a future date to give an expiration to the session.',
  `session_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Large to be able to insert alot of data to session array'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Stores PHP session';

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` mediumint UNSIGNED NOT NULL,
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Encoded with password_hash(PASSWORD_DEFAULT)',
  `registration_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login_datetime` datetime DEFAULT NULL,
  `email_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` smallint UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 = user, 1 = super admin, 2 = admin...'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `current_round_defender` (`current_defender_id`),
  ADD KEY `current_attacker_id` (`current_attacker_id`);

--
-- Indexes for table `game_deck`
--
ALTER TABLE `game_deck`
  ADD PRIMARY KEY (`game_id`);

--
-- Indexes for table `game_log`
--
ALTER TABLE `game_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id` (`game_id`),
  ADD KEY `game_log_ibfk_2` (`user_id`);

--
-- Indexes for table `game_player`
--
ALTER TABLE `game_player`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email_addr` (`email_addr`(100)) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `game_log`
--
ALTER TABLE `game_log`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Use this ID as the key for AJAX requests to indicate what client / user has as the last piece of the story';

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`current_defender_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `game_ibfk_2` FOREIGN KEY (`current_attacker_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `game_deck`
--
ALTER TABLE `game_deck`
  ADD CONSTRAINT `game_deck_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `game_log`
--
ALTER TABLE `game_log`
  ADD CONSTRAINT `game_log_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `game_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `game_player`
--
ALTER TABLE `game_player`
  ADD CONSTRAINT `game_player_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `game_player_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
