<?php
/** 
 * All requests to non-existing files are redirected to this file
*/

$start_time = microtime(true);
require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'fool_private_app' . DIRECTORY_SEPARATOR . 'bootstrap.php'; // Access private resources

// Data for navigation purposes
$smarty->assign('show_admin_panel', $role->checkRole(\Core\Role::ADMIN | \Core\Role::SUPER_ADMIN) ? true : false);

$router = new \Core\Router($_SERVER['REQUEST_URI'], '/index.php/');
if (!$router->loadController()) {
    require_once 'error.php';
}
