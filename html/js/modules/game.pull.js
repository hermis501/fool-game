const AJAX_PATH = '/ajax/';
const AJAX_GAME_PATH = AJAX_PATH + 'game/';

// How frequently info should be pulled from the server
const PULLING_INTERVAL_IN_PROGRESS = 1500; // 1.5 s
const PULLING_INTERVAL_WAITING_FOR_PLAYERS = 2000; // 2 s
var timer = null; // Keep it's ID to be able to clear the autocall

// Check current game status and show user appropriate menu
function pullActOnStatus() {
    if (timer != null) {
        clearInterval(timer);
        timer = null;
    }

    $.post(
        AJAX_GAME_PATH + 'get-status',
        {},
        function (response, status) {
            if (!response || response.status !== 'OK') {
                alert('Game error: ' + JSON.stringify(response));
                return;
            }

            switch (response.message) {
                // Server says that current user does not belong to any games
                case '':
                    showMainMenu();
                    break;

                // Waiting for players to join the active table
                case 'waiting_for_players':
                    showWaitingMenu(response.userId, response.masterId);
                    pullWaitingForPlayers();
                    timer = setInterval(pullWaitingForPlayers, PULLING_INTERVAL_WAITING_FOR_PLAYERS);
                    break;

                // Game is in progress
                case 'in_progress':
                case 'finished': // Someone won
                    showGameProgressMenu();
                    pullGameProgress();
                    timer = setInterval(pullGameProgress, PULLING_INTERVAL_IN_PROGRESS);
                    break;

                case 'not_exist':
                    showMainMenu();
                    break;

                default: // Unknown status
                    alert('Unknown status error, this should be fixed');
                    showMainMenu();
                    break;
            }
        },
        "json"
    );
}

function pullWaitingForPlayers() {
    $.post(
        AJAX_GAME_PATH + 'waiting-for-players',
        { lrkey: $('#lrkey').val() },
        function (response, status) {
            if (!response || response.status != 'OK') {
                return;
            }

            if (response.message.status != 'waiting_for_players') {
                pullActOnStatus();
                return;
            }

            $('#lrkey').val(response.message.lrkey);

            var history = '';
            var chatMessages = '';
            var lastChatMessage = '';
            if (response.message.log) {
                $.each(response.message.log, function (i, log) {
                    if (log.action !== 'chat') {
                        history += translateLogItem(log, response.message) + "\n";
                    } else {
                        chatMessages += translateChatItem(log);
                        lastChatMessage = log.username + ' says: ' + log.data;
                    }
                });
                if (chatMessages.length > 0) {
                    addToChat(chatMessages);
                    $('#screen-reader-only').text(lastChatMessage);
                }
            }

            // Check for reconnecting players
            if (brokenPlayers.length > 0) {
                if (!response.message.brokenPlayers) {
                    response.message.brokenPlayers = [];
                }
                $.each(brokenPlayers, function (i, username) {
                    if ($.inArray(username, response.message.brokenPlayers) === -1) {
                        history += username + ' reconnected.' + "\n";
                        brokenPlayers.splice(i, 1);
                        playSound('reconnected.wav', true);
                    }
                });
            }

            // Check for broken players
            if (response.message.brokenPlayers) {
                $.each(response.message.brokenPlayers, function (i, username) {
                    if ($.inArray(username, brokenPlayers) === -1) {
                        history += username + ' has suddenly lost internet connection.' + "\n";
                        playSound('lost_connection.wav', true);
                    }
                });
                brokenPlayers = response.message.brokenPlayers;
            }

            if (history.length > 0) {
                addToLog(history);
            }
        },
        "json"
    );

}

// Handles all the game process
function pullGameProgress() {
    $.post(
        AJAX_GAME_PATH + 'in-progress',
        { lrkey: $('#lrkey').val() },
        function (response, status) {
            if (!response || response.status != 'OK') {
                clearInterval(timer);
                alert('Game pull error: ' + JSON.stringify(response));
                return;
            }

            $('#lrkey').val(response.message.lrkey);

            if (response.message.status === 'not_exist') {
                clearChat();
                $('#game-info-wrapper').addClass('d-none');
                $('#trump-card').html('');
                $('#cards-in-play-wrapper').addClass('d-none');
                $('#cards-in-hand-wrapper').addClass('d-none');
                brokenPlayers = [];
                clearInterval(timer);
                addToLog('Table was closed.');
                pullActOnStatus();
                return;
            }

            if (response.message.nothingChanged && brokenPlayers.length === 0 && !response.message.brokenPlayers) { // There are no broken players and nothing had changed
                return;
            }

            // If trump is not revealed yet, do so
            if (response.message.trumpCard && $('#trump-card').html() == '') {
                var trumpImg = '<img id="trump-card-img" src="/images/cards/' + response.message.trumpCard + '.png" alt="Trump: ' + getCardName(response.message.trumpCard) + '" />';
                $('#trump-card').html(trumpImg);
            }

            // Update deck counter
            if (typeof response.message.cardsInDeck != typeof undefined && $('#cards-in-deck').html() != response.message.cardsInDeck) {
                $('#cards-in-deck').html('Cards in deck: ' + response.message.cardsInDeck);
            }

            // Update players if needed
            if (response.message.gamePlayers) {
                $.each(response.message.gamePlayers, function (i, player) {
                    var itemContent = player.username + ' (' + player.cards_in_hand + ')';
                    var listItem = '<li id="player-' + player.user_id + '">' + itemContent + '</li>';
                    if ($('#player-' + player.user_id).length == 0) {
                        $('#players ul').append(listItem);
                    } else if ($('#player-' + player.user_id).html() != itemContent) {
                        $('#player-' + player.user_id).html(itemContent);
                    }
                });
            }

            // If there are cards on the table
            if (response.message.cardsInPlay) {
                updateCardsInPlay(response.message.cardsInPlay);
            } else if (!response.message.nothingChanged) { // Server asks to clear cards from table
                $('#cards-in-play-atk').html('');
                $('#cards-in-play-def').html('');
                $('#cards-in-play-table').css('margin-bottom', '0');
            }

            // Update cards in hand
            if (response.message.playerCards) {
                updatePlayerCards(response.message.playerCards);
            }

            // Update log and turns
            var history = '';
            var chatMessages = '';
            var lastChatMessage = '';
            var anounceTurn = false;
            if (response.message.log) {
                $.each(response.message.log, function (i, log) {
                    if (log.action !== 'chat') {
                        history += translateLogItem(log, response.message) + "\n";
                        anounceTurn = true;
                    } else {
                        chatMessages += translateChatItem(log);
                        lastChatMessage = log.username + ' says: ' + log.data;
                    }
                });
                if (chatMessages.length > 0) {
                    addToChat(chatMessages);
                    $('#screen-reader-only').html(lastChatMessage);
                }

                // Determine who's turn
                if (response.message.status !== 'finished' && response.message.currentTurnPlayerId && response.message.currentTurnPlayerUsername) {
                    $('#players ul li').removeClass('current-turn');
                    $('#player-' + response.message.currentTurnPlayerId).addClass('current-turn');
                    if (anounceTurn) {
                        if (response.message.userId === response.message.currentTurnPlayerId) {
                            history += "It's your turn.\n";
                        } else {
                            history += "It's " + response.message.currentTurnPlayerUsername + "'s turn.\n";
                        }
                    }
                }
            }

            // Check for reconnecting players
            if (brokenPlayers.length > 0) {
                if (!response.message.brokenPlayers) {
                    response.message.brokenPlayers = [];
                }
                $.each(brokenPlayers, function (i, username) {
                    if ($.inArray(username, response.message.brokenPlayers) === -1) {
                        history += username + ' reconnected.' + "\n";
                        brokenPlayers.splice(i, 1);
                        playSound('reconnected.wav', true);
                    }
                });
            }

            // Check for broken players
            if (response.message.brokenPlayers) {
                $.each(response.message.brokenPlayers, function (i, username) {
                    if ($.inArray(username, brokenPlayers) === -1) {
                        history += username + ' has suddenly lost internet connection.' + "\n";
                        playSound('lost_connection.wav', true);
                    }
                });
                brokenPlayers = response.message.brokenPlayers;
            }

            if (history.length > 0) {
                if (chatMessages.length === 0) {
                    addToLog(history);
                } else {
                    addToLog(history, false);
                }
            }

            // Reviel info
            $('#game-info-wrapper').removeClass('d-none');
            $('#cards-in-play-wrapper').removeClass('d-none');
            $('#cards-in-hand-wrapper').removeClass('d-none');

        },
        "json"
    );

}

function translateLogItem(logObj, gameObj) {
    var logMessage = '';
    switch (logObj.action) {
        case 'created_game':
            if (logObj.user_id === gameObj.userId) {
                logMessage += 'You created table. Wait for players to join, then start the game and have some fun!';
            } else {
                logMessage += logObj.username + ' created new table.';
            }
            break;

        case 'joined_game':
            if (logObj.user_id === gameObj.userId) {
                logMessage += 'You joined the table.';
            } else {
                logMessage += logObj.username + ' has joined the table.';
            }
            playSound('joined_game.mp3');
            break;

        case 'left_game':
            if (logObj.user_id === gameObj.userId) {
                clearLog();
                logMessage += 'You left the table.';
            } else {
                logMessage += logObj.username + ' has left the table.';
            }
            playSound('left_game.mp3', true);
            break;

        case 'started_game':
            clearLog();
            logMessage += "Game started. Deck was shuffled and 6 cards were dealt to each player.\n";
            logMessage += 'Card at the bottom of the deck was drawn and placed as a trump for the rest of this game. It is ' + getCardName(gameObj.trumpCard) + '.';
            playSound('shuffle_cards.wav', true);
            break;

        case 'played_card_atk':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You atack with ' + getCardName(logObj.data) + '.';
            } else {
                logMessage += logObj.username + ' atacks with ' + getCardName(logObj.data) + '.';
            }
            break;

        case 'played_card_def':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You defend with ' + getCardName(logObj.data) + '.';
            } else {
                logMessage += logObj.username + ' defends with ' + getCardName(logObj.data) + '.';
            }
            break;

        case 'played_card_skip':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You skip the turn by playing ' + getCardName(logObj.data) + '.';
            } else {
                logMessage += logObj.username + ' skips the turn by playing ' + getCardName(logObj.data) + '.';
            }
            break;

        case 'passed_turn':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You pass.';
            } else {
                logMessage += logObj.username + ' passes.';
            }
            playSound('passed_turn.wav', true);
            break;

        case 'taking_home':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'Wait while others will throw more cards to take.';
            } else {
                logMessage += logObj.username + ' is taking cards home. Now all players can throw additional cards to the defender.';
            }
            playSound('taking_home.wav', true);
            break;

        case 'played_card_home':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You throw ' + getCardName(logObj.data) + ' to be taken home.';
            } else {
                logMessage += logObj.username + ' throws ' + getCardName(logObj.data) + ' to be taken home.';
            }
            break;

        case 'took_home':
            clearLog();
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You took all cards from the table.';
            } else {
                logMessage += logObj.username + ' took cards home.';
            }
            playSound('took_home.wav', true);
            break;

        case 'bat':
            clearLog();
            if (logObj.username === logObj.data) { // The same player invoked bat
                if (gameObj.userId === logObj.user_id) {
                    logMessage += 'You defended successfuly.';
                } else {
                    logMessage += logObj.data + ' has defended successfuly.';
                }
            } else {
                if (gameObj.userId === logObj.user_id) {
                    logMessage += 'You pass, so ' + logObj.data + ' has defended successfuly.';
                } else if (gameObj.username === logObj.data) {
                    logMessage += logObj.username + ' passes, so you defended successfuly.';
                } else {
                    logMessage += logObj.username + ' passes, so ' + logObj.data + ' has defended successfuly.';
                }
            }
            logMessage += ' Cards from the table were thrown to the discard pile. New cards were delt to players, round begins!';
            playSound('bat.mp3', true);
            break;

        case 'won':
            if (gameObj.userId === logObj.user_id) {
                logMessage += 'You won the game. Congratulations!';
                playSound('won.mp3', true);
            } else {
                logMessage += logObj.username + ' won the game!';
                playSound('lost.wav', true);
            }
            break;

        case 'draw':
            logMessage += 'Since more than one player left without the cards during last round, we think it is a draw!' + "\n";
            logMessage += 'Winners of this game are: ' + logObj.data.replace(/\,/g, ', ') + '.' + "\n";
            playSound('draw.wav', true);
            break;

        default:
            logMessage += 'Unknown action';
    }

    return logMessage;
}

function translateChatItem(logObj) {
    var chatMsg = logObj.username + ' says: ' + logObj.data + "\n";
    return chatMsg;
}

function addToChat(newMessages) {
    var chatMessages = $('#chat-content').val();
    chatMessages += newMessages;
    $('#chat-content').val(chatMessages);
    $('#chat-content').scrollTop($('#chat-content')[0].scrollHeight - $('#chat-content').height());
    playSound('chat.wav', true);
}

function getCardName(cardCode) {
    const suits = { C: 'clubs', D: 'diamonds', H: 'hearts', S: 'spades' };
    const ranks = {
        1: '10',
        2: '2',
        3: '3',
        4: '4',
        5: '5',
        6: '6',
        7: '7',
        8: '8',
        9: '9',
        J: 'Jack',
        Q: 'Queen',
        K: 'King',
        A: 'Ace'
    };

    if (ranks[cardCode[0]] && suits[cardCode[1]]) {
        return ranks[cardCode[0]] + ' of ' + suits[cardCode[1]];
    } else {
        return 'unknown';
    }

}