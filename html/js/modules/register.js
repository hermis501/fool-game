/** 
 * Registration form validation
*/

jQuery(document).ready(function ($) {
    var form_fields_required = ['username', 'email', 'password', 'confirm-password'];
    var form_group = 'div.form-group'; // Ancestor of err-msg block near the current field
    var form_input = '#register-form input';
    var form_submit_button = '#register-form input[type="submit"]';
    var errs_msg = 'div.err-msg';
    if ($(form_submit_button).hasClass('disabled') == false) {
        $(form_submit_button).addClass('disabled');
        $(form_submit_button).prop('disabled', 'disabled');
    }
    if ($(errs_msg).hasClass('d-none') == false) {
        $(errs_msg).addClass('d-none');
        $(errs_msg).text('');
    }

    $(form_input).on('input', function (event) {
        if ($(form_submit_button).hasClass('disabled') == false) { // User changed input, so we do not let to submit the form before validation occurs
            $(form_submit_button).addClass('disabled');
            $(form_submit_button).prop('disabled', 'disabled');
        }
        var err_field = $(this).closest(form_group).find(errs_msg); // Current field (which got an input)
        if ($(err_field).hasClass('d-none') == false) {
            $(err_field).addClass('d-none');
            $(err_field).text('');
        }

        try {
            if ($(this).val() == '') {
                throw 'Field can not be empty.';
            }

            switch (event.target.id) {
                case 'username':
                    if ($('#username').val().length < 3) {
                        throw 'Username is to short. At least 3 characters.';
                    }
                    var username_regex = /^[A-Za-z0-9]*$/;
                    if (username_regex.test($('#username').val()) == false) {
                        throw 'Username can consist of latin letters and arabic digits.';
                    }
                    break;

                case 'email':
                    var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (email_regex.test($('#email').val()) == false) {
                        throw 'Email address is invalid.';
                    }
                    break;

                case 'password':
                    if ($('#password').val().length < 6) {
                        throw 'Password is to short. It should consist of at least 6 characters.';
                    }
                    if ($('#password').val().length > 70) {
                        throw 'Password is to long. It can be maximum 70 characters of length.';
                    }
                    
                    if ($('#password').val() !== $('#confirm-password').val()) {
                        err_field = $('#confirm-password').closest(form_group).find(errs_msg); // Add error after confirm-password field
                        throw 'Passwords do not match.';
                    }
                    break;

                case 'confirm-password':
                    if ($('#password').val() !== $('#confirm-password').val()) {
                        throw 'Passwords do not match.';
                    }
                    break;
            }

            // Check if there are some fields that should not be marked as errors
            for (var i = 0; i < form_fields_required.length; i++) {
                var err_current_field = $('#' + form_fields_required[i]).closest(form_group).find(errs_msg);
                if ($(err_current_field).text() == '' && $(err_current_field).hasClass('d-none') == false) { // Error is empty and it's not hidden
                    $(err_current_field).addClass('d-none');
                }
            }

            // Check if all fields not empty and there are no errors
            for (var i = 0; i < form_fields_required.length; i++) {
                var err_current_field = $('#' + form_fields_required[i]).closest(form_group).find(errs_msg);
                if ($(err_current_field).text() != '') { // Error's still left
                    return false;
                }
                if ($('#' + form_fields_required[i]).val() == '') { // Field is empty
                    return false;
                }
            }
            // Fields aren't empty, proceed and let to submit form
            $(form_submit_button).prop('disabled', false);
            $(form_submit_button).removeClass('disabled');
            return true;
        } catch (err) {
            $(err_field).removeClass('d-none');
            $(err_field).text(err);
            return false;
        }
    });

});