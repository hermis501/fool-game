var brokenPlayers = [];
var gamesTimer = null;
var gamesListChanged = true;

// Needed for sounds (user interaction required)
let isInteracting = false;
['click', 'mousemove', 'mouseover', 'touchmove'].forEach((eventName) => {
    window.addEventListener(eventName, () => {
        if (!isInteracting) {
            isInteracting = true;
        }
    });
});

jQuery(document).ready(function ($) {
    pullActOnStatus(); // Get info from the server to determine what to do further

    $('#chat-form').submit(function (event) {
        event.preventDefault();

        $.post(
            '/ajax/game/send-message',
            { message: $('#chat-message').val() },
            function (response, status) {
                if (!response) {
                    return false;
                }
                if (response.status === 'ERROR') {
                    addToLog(response.message);
                    return false;
                }

                $('#chat-message').val('');
                return false;
            },
            "json"
        );
    });

}); // End of jQuery ready

function createGame() {
    $.post(
        '/ajax/game/create',
        {},
        function (response, status) {
            if (response && response.status == 'OK') { // Requested server to create new table
                playSound('joined_game.mp3');
                pullActOnStatus();
            }
        },
        "json"
    );
}

function leaveGame() {
    clearInterval(timer);
    if (!window.confirm('Are you sure you want to leave the table? If game is in progress, it  will be canseled and others might be mad about it!')) {
        pullGameProgress();
        return;
    }

    $.post(
        '/ajax/game/leave',
        {},
        function (response, status) {
            if (!response) {
                return;
            }

            if (response.status == 'OK') { // User has left the game
                clearChat();
                $('#game-info-wrapper').addClass('d-none');
                $('#cards-in-play-table').addClass('d-none');
                $('#cards-in-hand-wrapper').addClass('d-none');
                brokenPlayers = [];
                clearLog();
                addToLog('You left the table.');
                showMainMenu();
            }
        },
        "json"
    );
}

// Requests the tables from the server each 5 s
function gamesList() {
    $('#actions').html('');
    addToLog('Wait for new tables to appear or join the existing tables.');
    gamesListChanged = true;
    pullGamesList();
    gamesTimer = window.setInterval(pullGamesList, 5000);
}

function pullGamesList() {
    $.post(
        '/ajax/game/games-list',
        {},
        function (response, status) {
            if (!response) {
                window.clearInterval(gamesTimer);
                return;
            }

            if (response.status == 'ERROR' && gamesListChanged) {
                $('#actions').html('');
            }

            if ($('#go-back').length == 0) {
                var goBackBtn = $('<button type="button" id="go-back" class="list-group-item">Go back</button>' + "\n");
                $('#actions').append(goBackBtn);
                $('#go-back').on('click', showMainMenu);
            }

            if (response.status == 'ERROR') {
                if (gamesListChanged) {
                    addToLog(response.message);
                    gamesListChanged = false;
                    $('#go-back').focus();
                }

                return;
            }

            if (response.status == 'OK' && Array.isArray(response.message)) {
                var listItem = null;
                var allGames = [];
                $.each(response.message, function (i, item) {
                    var gameId = item.game_id;
                    allGames.push('game-' + gameId);
                    var text = 'Table with ' + item.players.replace(/\,/g, ', '); // Players with spaces after their usernames
                    listItem = $('<button type="button" id="game-' + gameId + '" class="list-group-item">' + text + '</button>' + "\n");
                    if ($('#game-' + gameId).length == 0) {
                        $('#actions').prepend(listItem);
                        $('#game-' + gameId).on('click', joinGame);
                        gamesListChanged = true;
                    } else if ($('#game-' + gameId).text() != text) {
                        $('#game-' + gameId).text(text);
                    }
                });

                $('#actions button').each(function (i, item) {
                    if ($.inArray($(item).attr('id'), allGames) === -1 && $(item).attr('id') != 'go-back') {
                        $(item).remove();
                        gamesListChanged = true;
                    }
                });

                if (gamesListChanged) {
                    $(listItem).focus();
                }
            }
        },
        "json"
    );
}

function showMainMenu() {
    if (timer != null) {
        window.clearInterval(timer);
    }
    if (gamesTimer != null) {
        window.clearInterval(gamesTimer);
    }

    $('#actions').addClass('d-none');
    actions = '<button type="button" id="create-game" class="list-group-item">Create new table</button>' + "\n";
    actions += '<button type="button" id="games-list" class="list-group-item">Join the table</button>' + "\n";
    actions += '<button type="button" id="logout" class="list-group-item">Logout</button>' + "\n";
    $('#actions').html(actions);
    $('#create-game').on('click', createGame);
    $('#games-list').on('click', gamesList);
    $('#logout').on('click', logout);
    window.setTimeout(function () {
        $('#actions').removeClass('d-none');
        $('#create-game').focus();
    }, 50);
}

function showWaitingMenu(userId, masterId) {
    $('#chat-wrapper').removeClass('d-none');
    $('#actions').addClass('d-none');
    var actions = '';
    if (masterId == null || masterId == userId) { // Show start game button if current player is the master of the table
        actions += '<button id="start-game" type="button" class="list-group-item">Start game</button>' + "\n";
    }
    actions += '<button id="leave-game" type="button" class="list-group-item">Leave table</button>' + "\n";
    $('#actions').html(actions);
    if (masterId == null || masterId == userId) {
        $('#start-game').on('click', startGame);
    }
    $('#leave-game').on('click', leaveGame);
    setTimeout(function () {
        $('#actions').removeClass('d-none');
        if (masterId == null || masterId == userId) {
            $('#start-game').focus();
        } else {
            $('#leave-game').focus();
        }
    }, 50);
}

function joinGame(event) {
    var gameId = event.target.id.replace('game-', ''); // Get actual game ID
    $.post(
        '/ajax/game/join/' + gameId,
        {},
        function (response, status) {
            if (!response) {
                return;
            }

            if (response.status === 'ERROR') {
                addToLog(response.message);
                return;
            }

            if (response.status == 'OK') {
                window.clearInterval(gamesTimer);
                $('#lrkey').val(response.lrkey);
                pullActOnStatus();
            }
        },
        "json"
    );
}

function logout() {
    if (window.confirm('Are you sure you want to logout from the game?')) {
        window.location.replace('/logout');
    } else {
        return;
    }
}

function startGame() {
    $.post(
        '/ajax/game/start',
        {},
        function (response, status) {
            if (!response) {
                return;
            }

            if (response.status == 'ERROR') {
                addToLog(response.message);
                return;
            }

            if (response.status == 'OK') {
                clearLog();
                playSound('shuffle_cards.mp3');
                pullActOnStatus();
            }
        },
        "json"
    );
}

function showGameProgressMenu() {
    $('#chat-wrapper').removeClass('d-none');
    $('#cards-in-hand').html('');
    var cardsInPlayElem = '<table id="cards-in-play-table" role="grid">';
    cardsInPlayElem += '<caption role="heading" aria-level="1">Cards in play</caption>';
    cardsInPlayElem += '<tr id="cards-in-play-atk" role="row"></tr>';
    cardsInPlayElem += '<tr id="cards-in-play-def" role="row"></tr>';
    cardsInPlayElem += '</table>';
    $('#cards-in-play-wrapper').html(cardsInPlayElem);
    var playersList = $('<ul></ul>');
    $('#players').html(playersList);
    $('#trump-card').html('');
    var actions = '';
    actions += '<button id="pass-turn" type="button" class="list-group-item">Pass</button>' + "\n";
    actions += '<button id="take-home" type="button" class="list-group-item">Take home</button>' + "\n";
    actions += '<button id="leave-game" type="button" class="list-group-item">Leave table</button>' + "\n";
    $('#actions').html(actions);
    $('#pass-turn').on('click', passTurn);
    $('#take-home').on('click', takeHome);
    $('#leave-game').on('click', leaveGame);
    setTimeout(function () {
        $('#actions').removeClass('d-none');
        $('#pass-turn').focus();
    }, 50);

}

function passTurn() {
    $.post(
        '/ajax/game/pass-turn',
        {},
        function (response, status) {
            if (!response) {
                alert('turn pass error');
                return;
            }

            if (response.status == 'ERROR') {
                addToLog(response.message);
                return;
            }

            if (response.status == 'OK') {
                pullGameProgress();
            }
        },
        "json"
    );
}

function takeHome() {
    $.post(
        '/ajax/game/take-home',
        {},
        function (response, status) {
            if (!response) {
                alert('Take home error');
                return;
            }

            if (response.status == 'ERROR') {
                addToLog(response.message);
                return;
            }

            if (response.status == 'OK') {
                pullGameProgress();
            }
        },
        "json"
    );
}

function updatePlayerCards(cards) {
    $.each(cards, function (id, name) {
        cardId = 'cards-in-hand-' + id;
        if ($('#' + cardId).length == 0) {
            var card = $('<div class="card-in-hand"><img id="' + cardId + '" role="button" src="/images/cards/' + id + '.png" alt="' + getCardName(id) + '" /></div>');
            $('#cards-in-hand').append(card);
            $('#' + cardId).on('click', playCard);
        }
    });
}

function updateCardsInPlay(cards) {
    var attackingCards = [];
    var atks = 0;
    var defs = 0;
    var row = 0;
    $.each(cards, function (i, card) {
        var action = card[0];
        var code = card[1];
        cardId = 'cards-in-play-' + code;
        if (action === 'played_card_atk' || action === 'played_card_skip' || action === 'played_card_home') {
            attackingCards.push(cardId);
            ++atks;
            row = 1;
        } else if (action === 'played_card_def') {
            ++defs;
            row = 2;
        }

        if ($('#' + cardId).length == 0) { // Card doesn't appear to client
            // append rows for each atack and defence set
            var tdContent = '<div class="card-in-play"><img src="/images/cards/' + code + '.png" alt="' + getCardName(code) + '" /></div>';
            if (row === 1) {
                var td = '<td id="cards-in-play-' + code + '" role="gridcell">' + tdContent + '</td>';
                $('#cards-in-play-atk').prepend(td);
                td = $('<td></td>').attr('id', 'cards-in-play-' + atks).attr('role', 'gridcell').html('<span class="visually-hidden">Place for defensive card</span>');
                $('#cards-in-play-def').prepend(td);
            } else if (row === 2) {
                $('#cards-in-play-' + defs).attr('id', 'cards-in-play-' + code).attr('role', 'gridcell').addClass('cards-in-play-overlap-top').addClass('card-transparent').html(tdContent);
                $('#' + attackingCards[defs - 1]).addClass('card-transparent');
                $('#cards-in-play-table').css('margin-bottom', '-50px');
            }
            if (action == 'played_card_atk' || action === 'played_card_home') {
                playSound('played_card_atk.wav', true);
            } else if (action == 'played_card_skip') {
                playSound('played_card_skip.wav', true);
            } else {
                playSound('played_card_def.wav', true);
            }
        }
    });
}

function playCard(event) {
    var cardId = event.target.id.replace('cards-in-hand-', ''); // Get actual card ID

    $.post(
        '/ajax/game/play-card/' + cardId,
        {},
        function (response, status) {
            if (!response) {
                return;
            }

            if (response.status != 'OK') {
                addToLog(response.message);
                return;
            }

            // Remove played card from player hand view
            $('#cards-in-hand-' + cardId).parent().remove();
            pullGameProgress();
        },
        "json"
    );

}

// Report info to the user (visually and with screen reader)
function addToLog(message, readOutLoud = true) {
    var history = $('#history').val();
    if (history.length > 0) {
        history += "\n";
    }
    history += message;
    history = history.replace("\n\n", "\n");
    $('#history').val(history);
        $('#history').scrollTop($('#history')[0].scrollHeight - $('#history').height());
    if (readOutLoud) {
        $('#screen-reader-only').text('');
        window.setTimeout(function () {
            message = message.replace("\n\n", '');
            $('#screen-reader-only').text(message);
        }, 100);
    }
}

function clearLog() {
    $('#history').val('');
    $('#screen-reader-only').text('');
}

function clearChat() {
    $('#chat-content').val('');
    $('#chat-wrapper').addClass('d-none');
    }
    
function playSound(src, checkLrkey = false) {
    if (!isInteracting || (checkLrkey && $('#lrkey').val() == '')) {
        return;
    }

    try {
        var sound = new Audio('/sounds/' + src);
        sound.play();
    } catch (e) { }
}