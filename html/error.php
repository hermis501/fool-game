<?php
/** 
 * All server errors are redirected to this file
 */

define('SERVER_ERROR', true);
require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'fool_private_app' . DIRECTORY_SEPARATOR . 'bootstrap.php'; // Access private resources

$error = isset($_GET['id']) ? $_GET['id'] : 404;
switch ($error) {
    case 400:
        $error_msg = 'Bad request.';
        break;
    case 401:
        $error_msg = 'Unauthorized access.';
        break;
    case 402: // payment
        $error_msg = 'Page not found. That is all we know.';
        break;
    case 403:
        $error_msg = 'Access forbidden.';
        break;
    case 404:
        $error_msg = 'Page not found. That is all we know.';
        break;
    case 408:
        $error_msg = 'Request time out.';
        break;
    case 500:
        $error_msg = 'Internal server error.';
        break;
    default:
        $error_msg = 'Page not found. That is all we know.';
}
$smarty->assign('error_message', $error_msg);
$smarty->display('errors' . DIRECTORY_SEPARATOR . 'server_errors.tpl');
